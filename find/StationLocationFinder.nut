class StationLocationFinder
{
    /**
     * Finds each tile on which a train station of the given size could be built, such that at least one tile of that station coincides with
     * a tile from the given list.
     *
     * @param   AIList  tile_id_list    All tiles for which coverage is good enough
     * @param   int     center_tile_id  A tile near the center of the tiles in the list, used for speed optimization. For example the tile
     *                                  of the industry for which tile_id_list is a list of accepting tiles.
     * @param   int     platforms       The number of platforms of the train station.
     * @param   int     length          The length of the tracks of the train station.
     *
     * @return  AIList  List containing all tile ids as in description as items, with as value a non zero combination of the
     *                  AIRail.RAILTRACK_NW_SE and AIRail.RAILTRACK_NE_SW flags
     */
    static function GetCanBuildList(tile_id_list, center_tile_id, platforms, length)
    {
        local result = AIList();
        local radius = AIStation.GetCoverageRadius(AIStation.STATION_TRAIN);
        local center_tile_x = AIMap.GetTileX(center_tile_id);
        local center_tile_y = AIMap.GetTileY(center_tile_id);
        
        foreach(tile_id, value in tile_id_list)
        {
            StationLocationFinder.IfCanBuildStationAddDirection(result, tile_id, AIRail.RAILTRACK_NW_SE, platforms, length);
            StationLocationFinder.IfCanBuildStationAddDirection(result, tile_id, AIRail.RAILTRACK_NE_SW, platforms, length);
            if(center_tile_x - AIMap.GetTileX(tile_id) >= radius)
            {
                StationLocationFinder.CheckBaseTileOverlappingOutsideStation(result, tile_id, AIRail.RAILTRACK_NE_SW, platforms, length, true, length);
                StationLocationFinder.CheckBaseTileOverlappingOutsideStation(result, tile_id, AIRail.RAILTRACK_NW_SE, platforms, length, true, platforms);
            }
            if(center_tile_y - AIMap.GetTileY(tile_id) >= radius)
            {
                StationLocationFinder.CheckBaseTileOverlappingOutsideStation(result, tile_id, AIRail.RAILTRACK_NW_SE, platforms, length, false, length);
                StationLocationFinder.CheckBaseTileOverlappingOutsideStation(result, tile_id, AIRail.RAILTRACK_NE_SW, platforms, length, false, platforms);
            }
        }
        
        return result;
    }
    
    
    /**
     * Finds each tile from the list where a station of the given size could be connected to one of the default station exits for that type
     * of station. Does not check whether that exit can be connected any further.
     *
     * @param   AIList  tile_list    The tiles to check
     * @param   int     platforms       The number of platforms of the train station.
     * @param   int     length          The length of the tracks of the train station.
     *
     * @return  AIList  List containing all tile ids on which a station of that size could be exited, with as value a non zero combination
     *                  of the AIRail.RAILTRACK_NW_SE and AIRail.RAILTRACK_NE_SW flags to indicate the direction of the station.
     */
    static function GetCanExitList(tile_list, platforms, length)
    {
        local result = AIList();
        
        foreach(tile, value in tile_list)
        {
            switch(platforms)
            {
                case 1:
                    /* Station oriented NW_SE */
                    local track_direction = AIRail.RAILTRACK_NW_SE;
                    if((tile_list.GetValue(tile) & track_direction) != 0)
                    {
                        local front_part_loc = PartLocation(TrackParts.SE_NW, tile);
                        StationLocationFinder.IfCanBuildNextPartLocationAddDirection(result, tile, track_direction, front_part_loc);
                        
                        local rear_tile = Tile.GetTileInDirection(tile, Direction.SOUTHEAST, length - 1);
                        local rear_part_loc = PartLocation(TrackParts.NW_SE, rear_tile);
                        StationLocationFinder.IfCanBuildNextPartLocationAddDirection(result, tile, track_direction, rear_part_loc);
                    }
                    
                    /* Station oriented NE_SW */
                    track_direction = AIRail.RAILTRACK_NE_SW;
                    if((tile_list.GetValue(tile) & track_direction) != 0)
                    {
                        local front_part_loc = PartLocation(TrackParts.SW_NE, tile);
                        StationLocationFinder.IfCanBuildNextPartLocationAddDirection(result, tile, track_direction, front_part_loc);
                        
                        local rear_tile = Tile.GetTileInDirection(tile, Direction.SOUTHWEST, length - 1);
                        local rear_part_loc = PartLocation(TrackParts.NE_SW, rear_tile);
                        StationLocationFinder.IfCanBuildNextPartLocationAddDirection(result, tile, track_direction, rear_part_loc);
                    }
            }
            // One track
                // Check the tiles on both sides for buildability (ignoring already built) of one of the three exit directions
            // Two tracks
                // Check both sides for placement of three consecutive double tracks of the right kind
                // In case of a line type track part, check the second one for buidability of a crossing
            //TODO think about what to do with stations with more than two tracks
        }
        
        return result;
    }
    
    
    /**
     * Checks the base tiles for a series of stations outside the list but overlapping with a tile on the list.
     *
     * @private
     */
    static function CheckBaseTileOverlappingOutsideStation(list, tile_id, track_direction, platforms, length, check_x, max)
    {
        for(local i = -1; i > -max; i--)
        {
            local outer_tile_id;
            if(check_x) outer_tile_id = tile_id + AIMap.GetTileIndex(i, 0);
            else outer_tile_id = tile_id + AIMap.GetTileIndex(0, i);
            if(AIMap.IsValidTile(outer_tile_id))
            {
                StationLocationFinder.IfCanBuildStationAddDirection(list, outer_tile_id, track_direction, platforms, length);
            }
        }
    }
    
    
    /**
     * If a station with the given number of platforms of the given length can be built at the given tile in the given direction, that tile-
     * direction combination is added to the list.
     *
     * @private
     */
    static function IfCanBuildStationAddDirection(list, tile_id, track_direction, platforms, length)
    {
        local mode = AITestMode();
        if(AIRail.BuildRailStation(tile_id, track_direction, platforms, length, AIStation.STATION_NEW)
            || AIError.GetLastError() == AIError.ERR_NOT_ENOUGH_CASH)
        {
            if(list.HasItem(tile_id)) list.SetValue(tile_id, list.GetValue(tile_id) | track_direction);
            else list.AddItem(tile_id, track_direction);
        }
    }
    
    /**
     * Tries to build all the next part locations of part_location. If any is successful, ORs track_direction with the value of tile in list.
     *
     * @private
     */
    static function IfCanBuildNextPartLocationAddDirection(list, tile, track_direction, part_loc)
    {
        local next_part_locs = part_loc.NextPartLocations();
        foreach(next_part_loc in next_part_locs)
        {
            if(TrackPartBuilder.TryBuildIgnoreMoneyAndExisting(next_part_loc))
            {
                /* Exit found */
                if(list.HasItem(tile)) list.SetValue(tile, list.GetValue(tile) | track_direction);
                else list.AddItem(tile, track_direction);
            }
        }
    }
}