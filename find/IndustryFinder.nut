class IndustryFinder
{
    /**
    * Find a sorted list of the industries that have the highest production of a certain cargo. Last month's production
    * is checked. If there are less than number industries producing this cargo, all of them are returned.
    *
    * @param    cargo_id    The ID of the cargo that the industry should produce
    * @param    number      The number of ranked industries to return
    *
    * @return   AIList
    */
    static function ByHighestProduction(cargo_id, number)
    {
        /* Get a list of industires producing this cargo */
        local list = AIIndustryList_CargoProducing(cargo_id);
        /* Valuate the list by production, sort and cut off */
        // In the current implementation it is automatically sorted correctly after valuating. Let's not rely on that..
        list.Valuate(AIIndustry.GetLastMonthProduction, cargo_id);
        list.Sort(AIList.SORT_BY_VALUE, false);
        list.KeepTop(number);
        return list;
    }
}