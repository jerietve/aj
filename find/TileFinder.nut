class TileFinder
{
    /**
    * Returns an AITileList containing all tile IDs of tiles on which a station can be built near a certain industry
    * such that this station will accept or supply a certain cargo cargo for this industry.
    *
    * @param    industry_id     The ID of the industry
    * @param    cargo_id        The ID of the cargo
    * @param    station_size_x  The 'width' of the station in x-direction
    * @param    station_size_y  The 'height' of the station in y-direction
    * @param    station_radius  The coverage radius of the station
    * @param    accepting       True to check for acceptance, false to check for supply
    *
    * @return   AITileList
    */
    static function InRangeNearIndustry(industry_id, cargo_id, station_size_x, station_size_y, station_radius, accepting)
    {
local before_tick = AIController.GetTick();
local before_ops = AIController.GetTick() * 10000 - AIController.GetOpsTillSuspend();
        /* A lower bound list (all these tiles are okay) and an upper bound list (no tiles not in this list are okay) */
        local lower_bound_list, upper_bound_list;
        if(accepting)
        {
            lower_bound_list = AITileList_IndustryAccepting(industry_id, station_radius);
            upper_bound_list = AITileList_IndustryAccepting(industry_id, station_radius + max(station_size_x, station_size_y));
        }
        else
        {
            lower_bound_list = AITileList_IndustryProducing(industry_id, station_radius);
            upper_bound_list = AITileList_IndustryProducing(industry_id, station_radius + max(station_size_x, station_size_y));
        }
        /* Get the tiles of which it needs to be checked whether or not they are okay */
        upper_bound_list.RemoveList(lower_bound_list);
        /* Check for all these tiles whether they will do and discard the ones that won't */
        if(accepting)
        {
            upper_bound_list.Valuate(AITile.GetCargoAcceptance, cargo_id, station_size_x, station_size_y, station_radius);
            upper_bound_list.RemoveBelowValue(8);
        }
        else
        {
            upper_bound_list.Valuate(AITile.GetCargoProduction, cargo_id, station_size_x, station_size_y, station_radius);
            upper_bound_list.KeepAboveValue(0);
        }
        /* Recombine with the lower bound */
        upper_bound_list.AddList(lower_bound_list);
        
        /* Time to try building, to get rid of impossible ones */
        local okay_list = upper_bound_list;
        local delete_list = AITileList();
        local deleted = false;
        local mode = AITestMode();
        for(local tile = okay_list.Begin(); !okay_list.IsEnd(); tile = okay_list.Next())
        {
            if(!AIRail.BuildRailStation(tile, AIRail.RAILTRACK_NE_SW, station_size_y, station_size_x, AIStation.STATION_NEW))
            {
                /* Didn't work. Why not? */
                local error = AIError.GetLastError();
                if(error == AIError.ERR_OWNED_BY_ANOTHER_COMPANY || error == AIStation.ERR_STATION_TOO_MANY_STATIONS_IN_TOWN)
                        delete_list.AddItem(tile, 0);
                else if(error == AIError.ERR_AREA_NOT_CLEAR)
                {
                    /* It's probable the industry itself is in the way. If so, delete this tile from the list */
                    for(local x = 0; x < station_size_x; x++)
                    {
                        for(local y = 0; y < station_size_y; y++)
                        {
                            if(AIIndustry.IsValidIndustry(AIIndustry.GetIndustryID(tile + AIMap.GetTileIndex(x, y))))
                            {
                                // AILog.Info("Deleting " + Tile.ToString(tile));
                                delete_list.AddItem(tile, 0);
                                deleted = true;
                                break;
                            }
                        }
                        if(deleted)
                        {
                            deleted = false;
                            break;
                        }
                    }
                }
            }
        }
        okay_list.RemoveList(delete_list);
local after_ops = AIController.GetTick() * 10000 - AIController.GetOpsTillSuspend();
local after_tick = AIController.GetTick();
AILog.Info("That took " + (after_ops - before_ops - 9) + " ops, also " + (after_tick - before_tick) + " ticks");
        return okay_list;
    }
}