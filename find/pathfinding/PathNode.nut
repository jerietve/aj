class PathNode
{
    _previous      = null;  //The parent node of this node
    _next          = null;  //Only used once a path is final
    _tile          = null;  //The tile of this node
    _length        = null;  //The lengh of the path from the source to _tile
    _g_score       = null;  //The number indicating the 'cost' from the source to this node
    _h_score       = null;  //The heuristic score of this node
    _direction     = null;  //This is the abstract direction from _previous._tile to _tile
    _bridge        = null;  //True if a bridge is required to cross _tile
    _dist_to_prev  = null;  //The distance from _tile to _previous._tile. Definition depends on setting method
    _dist_to_start = null;  //The distance from _tile to the root of the path's tile. Definition depends on setting
                            //method
    
    /**
    *   Creates a new PathNode
    *
    *   @param  PathNode    previous    The parent
    *   @param  TileIndex   tile        This node's tile
    */
    constructor(previous, tile)
    {
        this._previous = previous;
        this._tile = tile;
        if(previous == null) _length = 0;
        else _length = previous._length + 1;
    }
    
    
    /**
    *   Used for checking the equality of two PathNodes, instead of ==. 
    *
    *   @param  PathNode    other_node  The PathNode to compare to
    *
    *   @return boolean True if both PathNodes have the same tile and direction, false otherwise
    */
    function Equals(other_node)
    {
        return this._tile == other_node._tile && _direction == other_node._direction;
    }
    
    
    /**
    *   Sets next and the distance variables for every node in this path. Should be called on the last node.
    */
    function Finalize()
    {
        local current = this;
        /* Run back along the path setting the distance and the next variables */
        while(current._previous != null)
        {
            current._previous._next = current;
            current._dist_to_prev = Map.DistanceTiles(current._tile, current._previous._tile);
            current = current._previous;
        }
        /* Run forward along the path setting the distance to the start */
        current._dist_to_start = 0;
        while(current._next != null)
        {
            current = current._next;
            current._dist_to_start = current._previous._dist_to_start + current._dist_to_prev;
        }
    }
    
    
    /**
    *   Returns the node a distance at least goal_dist along the path from this node to the start of the path, or null
    *   if such a node is not found. Should only be called on finalised paths.
    *   
    *   @param  goal_dist       int         The distance we want to move away from this node
    *   @param  cross_bridge    array[1]    If the first element is true, no node halfway a bridge will be returned
    *
    *   @return PathNode. The first element of cross_bridge will be true if a long bridge was crossed, false otherwise.
    */
    function GetNodeAtLeastDistAlongPath(goal_dist, cross_bridge)
    {
        if(_dist_to_start < goal_dist) return null;
        local current = this;
        local bridge = false;
        local long_bridge = false;
        local dist = 0;
        while(current._previous != null)
        {
            dist = dist + current._dist_to_prev;
            current = current._previous;
            if(bridge && current._bridge) long_bridge = true;
            bridge = current._bridge;
            if(dist >= goal_dist && (!cross_bridge[0] || !current._bridge))
            {
                cross_bridge[0] = long_bridge;
                return current;
            }
        }
        //unreachable
        throw("Unreachable code reached in PathNode.GetNodeAtLeastDistAlongPath");
    }
    
    
    function ToString()
    {
        local result = "";
        if(_tile == null) result = result + "Tile: null";
        else result = result + "Tile: " + Tile.ToString(_tile)
        if(_g_score == null) result = result + ", G: null";
        else result = result + ", G: " + _g_score;
        if(_h_score == null) result = result + ", H: null";
        else result = result + ", H: " + _h_score;
        if(_h_score == null || _g_score == null) result = result + ", F: null";
        else result = result + ", F: " + (_g_score + _h_score);
        if(_previous == null || _previous._tile == null) result = result + ", Parent tile: null";
        else result = result + ", Parent tile: " + Tile.ToString(_previous._tile);
        return result;
    }
}