class RailPathNode
{
    _previous  = null;  //The parent node of this node
    _next      = null;  //Only used once a path is final
    _part_loc  = null;  //The track part under this node and its location
    _part_data = null;  //Information for special parts like bridges and tunnels
    _g_score   = null;  //The number indicating the 'cost' from the source to this node
    _h_score   = null;  //The heuristic score of this node
    
    /**
    *   Creates a new PathNode
    *
    *   @param  PathNode        previous    The parent
    *   @param  PartLocation    part_loc    This node's PartLocation
    */
    constructor(previous, part_loc)
    {
        this._previous = previous;
        this._part_loc = part_loc;
    }
    
    
    function GetFScore()
    {
        return _g_score + _h_score;
    }
    
    
    /**
    *   Sets next and the distance variables for every node in this path and returns the first node. Should be called on
    *   the last node.
    */
    function FinalizeAndReturnStart()
    {
        if(_previous == null) return this;
        local current = this;
        /* Run back along the path setting the next variables */
        while(true)
        {
            if(current._previous == null) return current;
            current._previous._next = current;
            current = current._previous;
        }
    }
    
    
    /**
    *   Connects the node that is other_path to the finish of this path
    */
    function Concatenate(other_path)
    {
        if(other_path == null)
        {
            AILog.Info("Trying to concatenate null path, ignoring");
            return;
        }
        local finish = GetFinish();
        finish._next = other_path;
        finish._next._previous = finish;
    }
    
    
    /**
    *   Connects the node that is other_path to the previous of the finish of this path, or to finish if no previous
    *   exists. This function must return its result because a node can't replace itself, which is necessary in case of 
    *   a path of length one.
    *
    *   @param  other_path  RailPathNode    The path to be concatenated to this
    *
    *   @return RailPathNode    The new path, at location this
    */
    function ConcatenateRemoveLast(other_path)
    {
        if(other_path == null)
        {
            AILog.Info("Trying to concatenate null path, ignoring");
            return;
        }
        local finish = GetFinish();
        if(finish._previous == null) return other_path;
        finish._previous._next = other_path;
        other_path._previous = finish._previous;
        return this;
    }
    
    
    /**
    *   Returns the last node reachable from this node by running along _previous
    */
    function GetStart()
    {
        local current = this;
        while(current._previous != null) current = current._previous;
        return current;
    }
    
    
    /**
    *   Returns the last node reachable from this node by running along _next
    */
    function GetFinish()
    {
        local current = this;
        while(current._next != null) current = current._next;
        return current;
    }
    
    
    function NumberOfNodes()
    {
        local current = GetStart();
        local amount = 1;
        while(current._next != null)
        {
            amount++;
            current = current._next;
        }
        return amount;
    }
    
    
    /**
    *   Used for checking the equality of two RailPathNodes, instead of ==. 
    *
    *   @param  PathNode    other_node  The RailPathNode to compare to
    *
    *   @return boolean True if both RailPathNodes have the same TTD, false otherwise
    */
    function Equals(other_node)
    {
        
        return other_node == null || this._part_loc.Equals(other_node._part_loc);
    }
    
    
    /**
    *   Prints this whole path
    */
    function Print()
    {
        AILog.Info("Printing path:");
        local current = this.GetStart();
        while(current != null)
        {
            AILog.Info(current.ToString());
            current = current._next;
        }
    }
    
    
    /**
    *   Returns a description of this node
    */
    function ToString()
    {
        return "RPN: H = " + _h_score + ", G = " + _g_score + ", F = " + GetFScore() + ", " + _part_loc.ToString();
    }
}