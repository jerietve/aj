class RailPathFinder
{
    /* Private */
    tp            = null;
    cheap_bridges = null;
    first_fixed   = null;
    test_mode     = null;
    E_W_TRACKBITS = null;
    N_S_TRACKBITS = null;
    
    constructor()
    {
        tp = ::ai_instance.tp;
        test_mode = AITestMode();
        E_W_TRACKBITS = AIRail.RAILTRACK_NW_NE | AIRail.RAILTRACK_SW_SE;
        N_S_TRACKBITS = AIRail.RAILTRACK_NE_SE | AIRail.RAILTRACK_NW_SW;
    }
    
    
    /**
    *   Finds an exact, seemingly buildable path between the two part locations, or at least to close_enough_dist from
    *   the second one.
    *
    *   @param  from_part_loc       PartLocation    The PartLocation from which to find a path.
    *   @param  to_part_loc         PartLocation    The PartLocation to which to find a path.
    *   @param  close_enough_dist   int             Finding a path to a tile this distance from the tile of the destination is good enough
    *   @param  cheap_bridges       boolean         If true, bridges are very easily incorporated in the path (use if you're certain you need them)
    *   @param  first_fixed         boolean         If true, from_part_loc cannot be replaced by a bridge
    *
    *   @return array   The RailPathNode containing the start of the path, or null in case it's not found
    */
    function FindPath(from_part_loc, to_part_loc, close_enough_dist, cheap_bridges, first_fixed)
    {
        AILog.Info("Finding path from " + from_part_loc.ToString() + " to " + to_part_loc.ToString());
        this.cheap_bridges = cheap_bridges;
        this.first_fixed = first_fixed;
        local closed_set = [];
        local open_set = Fibonacci_Heap();
        local current = RailPathNode(null, from_part_loc);
        current._g_score = 0;
        current._h_score = RailPathFinder.GetH(from_part_loc, to_part_loc);
        open_set.Insert(current, current._h_score);
                
        while(open_set.Count() > 0)
        {
            current = open_set.Pop();
            {
                local exec = AIExecMode();
            }
            if(current._part_loc.Equals(to_part_loc) || Map.DistanceTiles(current._part_loc.tile, to_part_loc.tile) < close_enough_dist)
            {
                return current.FinalizeAndReturnStart();
            }
            
            closed_set.append(current._part_loc);
            local next_node;
            local nexts_to_check;
            if(current._part_loc.part_id == TrackParts.BRIDGE_SEGMENT) nexts_to_check = current._part_data.end_part_loc.NextPartLocations();
            else nexts_to_check = current._part_loc.NextPartLocations();
            foreach(next in nexts_to_check)
            {
                if(next.Equals(to_part_loc))
                {
                    AILog.Info("Found it");
                    /* This needs to be handled separately because the build test will fail if there's already track there */
                    next_node = RailPathNode(current, next);
                    next_node._g_score = current._g_score + GetG(next_node, cheap_bridges);
                    next_node._h_score = 0;
                }
                else
                {
                    if(Util.InList(closed_set, next, false)) continue;
                    //See if we can build this
                    local fail = TryBuild(next);
                    //I thought if statements looked better than switch in this case
                    if(fail == 0) 
                    {
                        /* We can build it. Now that's easy */
                        next_node = RailPathNode(current, next);
                        next_node._g_score = current._g_score + GetG(next_node, cheap_bridges);
                        next_node._h_score = GetH(next_node._part_loc, to_part_loc);
                    }
                    else if(fail == 1)
                    {
                        /* Hopeless, nothing to be done */
                        continue;
                    }
                    else if(fail == 2)
                    {
                        /* It was suggested we try a bridge instead, starting at current. Bridges can only be built on
                            lines. Or maybe the bridge we got before turns out to drop us off in a place we can't continue
                            from so we should try a longer one */
                        if(!(TrackParts.IsLine(next.part_id) || TrackParts.IsBridge(next.part_id)) || first_fixed && current._g_score == 0) continue;
                        local bridge_data = FindBridgeSegment(current._part_loc);
                        if(bridge_data == null) continue; //Bridge can't be found, moving on
                        next.part_id = TrackParts.BRIDGE_SEGMENT;
                        next.tile = current._part_loc.tile;
                        next_node = RailPathNode(current._previous, next);
                        next_node._part_data = bridge_data;
                        if(current._previous == null) next_node._g_score = GetG(next_node, cheap_bridges);
                        else next_node._g_score = current._previous._g_score + GetG(next_node, cheap_bridges);
                        next_node._h_score = GetH(bridge_data.end_part_loc, to_part_loc);
                    }
                    else if(fail == 3)
                    {
                        /* It was suggested we try a bridge instead, starting at next */
                        local bridge_data = FindBridgeSegment(next);
                        if(bridge_data == null) continue; //Bridge can't be found, moving on
                        next.part_id = TrackParts.BRIDGE_SEGMENT;
                        next_node = RailPathNode(current, next);
                        next_node._part_data = bridge_data;
                        next_node._g_score = current._g_score + GetG(next_node, cheap_bridges);
                        next_node._h_score = GetH(bridge_data.end_part_loc, to_part_loc);
                    }
                    else if(fail == 4)
                    {
                        //TODO crossing might mean arrival?
                        continue;
                    }
                    //TODO demolish something, move the land
                }

                if(!open_set.Exists(next_node))
                {
                    local execmode = AIExecMode();
                    // if(next_node._part_loc.part_id == TrackParts.BRIDGE_SEGMENT)
                    // {
                        // AISign.BuildSign(next_node._part_data.end_part_loc.tile + tp.parts[next_node._part_data.end_part_loc.part_id].next_tile, "" + next_node.GetFScore());
                    // }
                    // else AISign.BuildSign(next_node._part_loc.tile + tp.parts[next_node._part_loc.part_id].next_tile, "" + next_node.GetFScore());
                    open_set.Insert(next_node, next_node.GetFScore());
                }
            }
        }
        
        return null;
    }
    
    
    function GetH(from_part_loc, to_part_loc)
    {
        return 2 * Map.DistanceTiles(tp.parts[from_part_loc.part_id].next_tile + from_part_loc.tile, to_part_loc.tile);
    }
    
    
    /**
    *   G score depends on the kind of trackpart only
    */
    function GetG(node, cheap_bridges)
    {
        if(node._part_loc.part_id == TrackParts.BRIDGE_SEGMENT)
        {
            //All simple rails count for one, bridges count for 2*length^1.2
            local sum = 0;
            foreach(track in node._part_data.track_descriptions)
            {
                foreach(number in track)
                {
                    if(number == 1) sum++;
                    else sum += pow(2*(number+1), 1.2);
                }
            }
            //AILog.Info("Bridge G cost = " + sum / (node._part_data.track_descriptions.len()));
            return sum / (node._part_data.track_descriptions.len());
        }
        else return tp.parts[node._part_loc.part_id].length;
    }
    
    
    /**
    *   Checks how long a bridge starting at tile in direction would need to be.
    *
    *   @param  tile        TileIndex   The start tile of the bridge
    *   @param  direction   Direction   The build direction of the bridge
    *
    *   @return int The length of the bridge if a bridge that long can be built, or -1 otherwise. The length is given as
    *   the distance from the start tile to the end tile.
    */
    function TryBridge(tile, direction)
    {
        /* The start should be buildable */
        if(!AITile.IsBuildable(tile)) return -1;
        local other_side;
        /*  Get the maximum bridge length. Add one because OpenTTD doesn't count the start and end tile, and this method
            counts the end tile */
        local max_bridge_length;
        if(AIGameSettings.IsValid("max_bridge_length")) max_bridge_length = AIGameSettings.GetValue("max_bridge_length") + 1;
        else max_bridge_length = 21; //I personally believe bridges longer than this are rediculous
        local test_mode = AITestMode();
        local bridge_list;
        
        for(local length = 2; length <= max_bridge_length + 1; length ++)
        {
            other_side = Tile.GetTileDirectionDistance(tile, direction, length);
            if(!AIMap.IsValidTile(other_side)) return -1;
            /* There are some quite difficult situations where building bridges is not allowed. For example with a
                height difference and a rail or road on a wall, or another bridge crossing our bridge. In order to avoid
                making mistakes trying to think of all possible situations, just try to build the bridge in test mode */
            /* Some tiles cannot be bridged. If we reach such a tile, a longer bridge can also never be built */
            if(!(AITile.IsBuildable(other_side) || AITile.IsWaterTile(other_side) || AIRoad.IsRoadTile(other_side) || AIRail.IsRailTile(other_side))) return -1;
            //Get a list of bridges for this length
            bridge_list = AIBridgeList_Length(length+1);
            //Simply try one
            if(AIBridge.BuildBridge(AIVehicle.VT_RAIL, bridge_list.Begin(), tile, other_side)) return length;
        }
        /* Maximum bridge length reached */
        return -1;
    }
    
    
    /**
    *   Tries to build the part at its location, and returns a code representing the proposed action. The action is a
    *   suggestion; it's not known whether or not this action will succeed. More suggestion codes can be added later.
    *
    *   @param  part_loc    PartLocation    The part and location to build at
    *
    *   @return int 0 in case the part can be built without problems
    *               1 in case the part can't be built and it's suggested to give up
    *               2 in case the part can't be built and it's suggested to start a bridge at the previous part
    *               3 in case the part can't be built and it's suggested to start a bridge at this part instead
    *               4 in case the part can be built but would cross your own rails
    */
    function TryBuild(part_loc)
    {
        local part = tp.parts[part_loc.part_id];
        local sections = part.sections;
        local test_mode = AITestMode();
        /* Try to build it and save the sections that didn't work */
        local success = true;
        local cross_rails = false;
        local coast = false;
        local tile;
        foreach(section in sections)
        {
            tile = section.offset + part_loc.tile;
            if(!AIRail.BuildRailTrack(tile, section.track))
            {
                success = false;
                /* Only lines can be replaced by bridges */
                if(!TrackParts.IsLine(part_loc.part_id)) return 1;
                /* Coast needs to be remembered because it could be worse, see next line */
                if(AITile.IsCoastTile(tile)) coast = true;
                /* If we ran into something that can be bridged, start a bridge from the previous part */
                if(AITile.IsWaterTile(tile) || AIRoad.IsRoadTile(tile)  || AIRail.IsRailTile(tile)) return 2;
                else return 1;
            }
            else if(AIRail.IsRailTile(tile) && !cross_rails)
            {
                /* There's rail here already, and it's ours. It's also not the same track as we're building because in
                    that case building would have failed. Find out if we're crossing it. Actually the only two cases in 
                    which we're not crossing it is if they are both diagonal tracks in the same direction */
                local railbits = AIRail.GetRailTracks(tile);
                if(!((section.track & E_W_TRACKBITS) != 0 && (railbits & E_W_TRACKBITS) != 0 || (section.track & N_S_TRACKBITS) != 0 && (railbits & N_S_TRACKBITS) != 0))
                {
                    cross_rails = true;
                }
            }
        }
        if(success)
        {
            /* Crossing our own rails, return 4 */
            if(cross_rails) return 4;
            /* Nothing went wrong, return the success code */
            else return 0;
        }
        else if(coast) return 3;
        throw(part_loc.ToString());
    }
    
    
    /**
    *   Starts a bridge from the tile(s) that need(s) to of part_loc and continues building (imaginary) bridges and
    *   flat rails alongside it on the other tracks that don't need bridges until a location is found where all tracks
    *   would no longer need to be a bridge. All this is nearly returned in a BridgeInformation instance
    *
    *   @param part_loc PartLocation    From where to start the first bridge. Must be a line part
    *
    *   @return BridgeInformation
    */
    function FindBridgeSegment(part_loc)
    {
        /* We're going in one direction and one direction only. */
        if(!TrackParts.IsLine(part_loc.part_id)) return null;
        local part = tp.parts[part_loc.part_id];
        local sections = part.sections;
        local direction = part.direction;
        /* Make an array per track to save the bridges and rails */
        local track_descriptions = array(sections.len());
        /* Dists will always contain for every track the distance from the start tile to the first tile that has not
            been filled with a rail or bridge yet */
        local dists = array(sections.len());
        /* Start the necessary bridges */
        local bridge_length;
        for(local i = 0; i < sections.len(); i++)
        {
            track_descriptions[i] = [];
            dists[i] = 0;
            /* If the next tile after the start of this track is not buildable */
            if(!AITile.IsBuildable(part_loc.tile + part.next_tile + sections[i].offset))
            {
                /* Find out how long a bridge from the very start would need to be */
                bridge_length = TryBridge(part_loc.tile + sections[i].offset, direction);
                if(bridge_length == -1) return null;
                track_descriptions[i].append(bridge_length);
                dists[i] = bridge_length + 1;
            }
        }
        while(!Util.AllTheSame(dists))
        {
            local current_track = Util.LowestValueIndex(dists);
            /* Find out if we need to start a bridge */
            if(!AITile.IsBuildable(Tile.GetTileDirectionDistance(part_loc.tile + sections[current_track].offset, direction, dists[current_track] + 1))
                    || AITile.IsCoastTile(Tile.GetTileDirectionDistance(part_loc.tile + sections[current_track].offset, direction, dists[current_track]))
                    && !(AIRail.BuildRailTrack(Tile.GetTileDirectionDistance(part_loc.tile + sections[current_track].offset, direction, dists[current_track]), sections[0].track)))
            {
                /* Find out how long a bridge would need to be */
                bridge_length = TryBridge(Tile.GetTileDirectionDistance(part_loc.tile + sections[current_track].offset, direction, dists[current_track]), direction);
                if(bridge_length == -1) return null;
                track_descriptions[current_track].append(bridge_length);
                dists[current_track] += bridge_length + 1;
            }
            else if(AITile.IsCoastTile(Tile.GetTileDirectionDistance(part_loc.tile + sections[current_track].offset, direction, dists[current_track]))
                    && !(AIRail.BuildRailTrack(Tile.GetTileDirectionDistance(part_loc.tile + sections[current_track].offset, direction, dists[current_track]), sections[0].track)))
            {
                
            }
            else
            {
                /* Just build one piece of track */
                track_descriptions[current_track].append(1);
                dists[current_track] += 1;
            }
        }
        
        /* Create the BridgeInformation */
        local bridge_info = BridgeInformation();
        bridge_info.start_part_loc = part_loc;
        bridge_info.end_part_loc = PartLocation(part_loc.part_id, Tile.GetTileDirectionDistance(part_loc.tile, direction, dists[0]-1));
        bridge_info.track_descriptions = track_descriptions;
        //AILog.Info("Suggested bridge from " + bridge_info.start_part_loc.ToString() + " to " + bridge_info.end_part_loc.ToString());
        
        return bridge_info;
    }
}