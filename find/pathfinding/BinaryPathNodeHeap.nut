class BinaryPathNodeHeap
{
    queue = null;
    count = 0;
    
    constructor()
    {
        queue = [];
    }
}


function BinaryPathNodeHeap::Insert(node, price)
{
    /* Append a dummy entry to be overwritten (just to make the array the right length */
    queue.append(0);
    count++;
    
    /* Find the place where the node should be inserted. The cheapeast node should be on top */
    local hole;
    for(hole = count - 1; hole > 0 && price <= queue[hole/2][1]; hole /= 2)
    {
        /* Move this node down */
        queue[hole] = queue[hole /2];
    }
    
    queue[hole] = [node, price];
}


function BinaryPathNodeHeap::Peek()
{
    return queue[0][0];
}


function BinaryPathNodeHeap::Pop()
{
    if(count == 0) return null;
    
    /* Remove the top node */
    local node = queue[0];
    /* Take the last node, put it on top and bubble down to repair the heap */
    queue[0] = queue[count - 1];
    queue.pop();
    count--;
    BinaryPathNodeHeap.DownHeap();
    
    return node[0];
}


function BinaryPathNodeHeap::Count()
{
    return count;
}


function BinaryPathNodeHeap::Exists(checknode)
{
    foreach(node in queue)
    {
        if(node[0].Equals(checknode)) return true;
    }
    
    return false;
}


function BinaryPathNodeHeap::Find(tile)
{
    foreach(node in queue)
    {
        if(node[0].GetTile() == tile) return tile;
    }
    
    return null;
}


function BinaryPathNodeHeap::DownHeap()
{
    if(count == 0) return;
    
    /* Min-Heapify */
    /* It's much easier if the array starts at 1 instead of 0. Add and subtract 1 where appropriate to establish this */
    local hole = 1;
    local root = queue[0];
    while(hole * 2 < count + 1)
    {
        /* Get the smaller of the children */
        local right = hole * 2 - 1;
        local left = right - 1;
        /* The right child might not exist */
        if(right == count)
        {
            /* Move on to the left one */
            if(queue[left][1] < root[1])
            {
                /* Switch the child up */
                queue[hole - 1] = queue[left];
                hole = left;
                /* Move to the next level */
                break;
            }
            /* End of the line */
            return;
        }
        /* So the right and left child both exist */
        if(queue[left][1] < queue[right][1])
        {
            if(queue[left][1] < root[1])
            {
                queue[hole] = queue[left];
                hole = left;
            }
        }
        else if(queue[right][1] < root[1])
        {
            queue[hole] = queue[right];
            hole = right;
        }
        else break;
    }
}


function BinaryPathNodeHeap::ToString()
{
    local string = "";
    foreach(node in queue)
    {
        string += node[1] + ",";
    }
    
    return string;
}