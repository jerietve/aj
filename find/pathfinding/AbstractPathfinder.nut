class AbstractPathfinder
{
    /* The open list should be a Fibonacci heap because it will get mostly insert and exists calls, and some popminimum and count calls */
    _open_list              = null;
    /* The closed list should just be an array because it will mostly get exists calls and if true those values will mostly have been added lately */
    _closed_list            = null;
    _sqrt2                  = sqrt(2);  
    _separation             = null;
    _max_bridge_length      = null;
    _dest_x                 = null;
    _dest_y                 = null;
    /* Heuristic settings */
    _cost_empty_tile        = null;
    _cost_height_difference = null;
    _cost_bridge            = null;
    
    /* Modules */
    _handle_water           = false;
    
    /* Lakes */
    _water_closed_list      = null;
    _around_list            = null;
    _initial_tile_x         = null;
    _initial_tile_y         = null;
    _initial_tile_h         = null;
    _bridge_vs_land         = null; //How much more expensive it is to build a bridge over getting out of your way to find a shorter bridge
    _bridge_list            = null;
    _going_left             = false;
    _left_tile              = null
    _left_direction         = null;
    _right_tile             = null;
    _right_direction        = null;

    constructor()
    {
        _open_list = Fibonacci_Heap();
        _closed_list = [];
        _separation = 4;//AIController.GetSetting("Path finder abstraction");
        if(AIGameSettings.IsValid("max_bridge_length")) _max_bridge_length = AIGameSettings.GetValue("max_bridge_length");
        else _max_bridge_length = 30;
        _around_list            = AITileList();
        
        /* Set the heuristic values */
        _cost_empty_tile        = 1.00;
        _cost_height_difference = 0;
        _cost_bridge            = 1.50;
        _bridge_vs_land         = 2.00;
    }
    

    /**
    *   Generates a path through the current map using A*, but only looking at a grid of tiles _separation apart. So if _separation is 4
    *   only one in every 4x4 tiles will be checked. This will generate a path at a higher abstraction level, that can be tried to be
    *   followed at a lower level later on. It is not guaranteed that this path can or could ever work. Very many things are ignored in order to come up with a
    *   path that is worth a try quickly. Another low-level pathfinder should fix all these problems and might not even be able to.
    *   
    *   @param  source      AITile  The source tile to start the path. Should not be a water tile
    *   @param  destination AITile  The destination tile to end the path
    *
    *   @return PathNode    The final node of the path, or null in case no path was found.
    */
    function GenerateAbstractPath(source, destination)
    {
        AILog.Info("Finding path from " + Tile.ToString(source) + " to " + Tile.ToString(destination));
        AILog.Info("Closed list len = " + _closed_list.len());
        foreach(node in _closed_list) AILog.Info("In list: " + Tile.ToString(node._tile) + ", " + node._direction);
        /* Shift destination to a location on the grid  in between the destination and the source */
        local dest_x_old = AIMap.GetTileX(destination);
        local dest_y_old = AIMap.GetTileY(destination);
        _dest_x = (dest_x_old / _separation) * _separation;
        _dest_y = (dest_y_old / _separation) * _separation;
        local destination = AIMap.GetTileIndex(_dest_x, _dest_y);
        
        /* Add the source to the open list */
        local source_x = AIMap.GetTileX(source);
        local source_y = AIMap.GetTileY(source);
        local node = PathNode(null, source);
        node._g_score = 0;
        node._h_score = HEstimate(source_x, source_y);
        node._bridge = false;
        _open_list.Insert(node, node._g_score + node._h_score);
        
        local debug = 0;
        
        /* Go on as long as there are tiles left in the open list */        
        while(_open_list.Count() > 0)
        {
            local current = _open_list.Pop();
            local mode = AIExecMode();
            // AILog.Info("Popped " + Tile.ToString(current._tile) + ", number " + debug + ", g = " + current._g_score + ", h = " + current._h_score + ", f = " + (current._h_score + current._g_score));
            // AISign.BuildSign(current._tile, "" + debug++);
            /* If we found the end, return this node (and the whole path with it) */
            if(AIMap.DistanceManhattan(current._tile, destination) < _separation) return current;
        
            if(AITile.IsWaterTile(current._tile) && _handle_water && current._previous != null)
            {
                /* Initialize the global variables */
                _initial_tile_x = AIMap.GetTileX(current._tile);
                _initial_tile_y = AIMap.GetTileY(current._tile);
                _initial_tile_h = current._h_score;
                _water_closed_list = [];
                _bridge_list = Fibonacci_Heap();
                AILog.Info("Bridge list length = " + _bridge_list.Count());
                AILog.Info("Handling lake");
                _left_tile = current._tile;
                _right_tile = current._tile;
                local direction = Direction.GetDirection(current._previous._tile, current._tile);
                _left_direction = direction;
                _right_direction = direction;
                local around_tile = HandleLake(current._tile, direction);
                AILog.Info("Bridge list length = " + _bridge_list.Count());
                if(around_tile != null)
                {
                    AILog.Info("Going around");
                    /* It was suggested to go around the lake. Try */
                    local small_finder = AbstractPathfinder();
                    /* Add the checked water tiles to the closed list of the small path finder */
                    foreach(node in _water_closed_list) small_finder._closed_list.append(node);
                    /* Find a path around the lake */
                    //TODO fix loop where two of these sub finders keep on trying to cross each other's lake
                    // Start a while back instead of at the coast
                    local break_node = StepBack(current, Map.DistanceTiles(current._previous._tile, around_tile) / _separation);
                    local final_node = small_finder.GenerateAbstractPath(break_node._tile, around_tile);
                    if(final_node == null)
                    {
                        /* No path was found. Add all closed water tiles to this path finder's closed list and move on */
                        foreach(node in _water_closed_list) _closed_list.append(node);
                        continue;
                    }
                    local path = final_node;
                    /* Run along the path, setting the right G and H values and adding them to the closed list*/
                    while(path._previous != null)
                    {
                        path._previous._h_score = HEstimate(AIMap.GetTileX(path._tile), AIMap.GetTileY(path._tile));
                        path._g_score += break_node._g_score;
                        _closed_list.append(path);
                        if(path._previous != null) path = path._previous;
                        else break;
                    }
                    /* Connect the paths */
                    path._previous = break_node;
                    /* Add the final node to the open list */
                    _open_list.Insert(final_node, final_node._g_score + final_node._h_score);
                }
                else
                {
                    AILog.Info("Trying bridge");
                    /* At least one bridge was found. Try to find a path to all bridges from the least costly one until a path is found */
                    while(_bridge_list.Peek() != null)
                    {
                        AILog.Info("Bridge list length = " + _bridge_list.Count());
                        local bridge = _bridge_list.Pop();
                        AILog.Info("Bridge starting at " + Tile.ToString(bridge._start_tile) + ", length " + bridge._length + ", direction " + bridge._direction);
                        AILog.Info("Distance from " + Tile.ToString(current._previous._tile) + " is " + Map.DistanceTiles(current._previous._tile, bridge._start_tile));
                        local small_finder = AbstractPathfinder();
                        /* Add the checked water tiles to the closed list of the small path finder */
                        foreach(node in _water_closed_list) small_finder._closed_list.append(node);
                        /* Find a path to the bridge */
                        //TODO fix loop where two of these sub finders keep on trying to cross each other's lake
                        // Start a while back instead of at the coast
                        local break_node = StepBack(current, Map.DistanceTiles(current._previous._tile, bridge._start_tile) / _separation);
                        local final_node = small_finder.GenerateAbstractPath(break_node._tile, bridge._start_tile);
                        if(final_node == null)
                        {
                            /* No path was found. Next bridge */
                            continue;
                        }
                        /* Add the tiles under the bridge to the path */
                        local under_bridge = PathNode(final_node, Tile.GetTileDirectionDistance(final_node._tile, bridge._direction, _separation));
                        under_bridge._bridge = true;
                        under_bridge._g_score = final_node._g_score + GEstimate(final_node, under_bridge);
                        for(local dist = _separation * 2; dist < bridge._length; dist += _separation)
                        {
                            local next_under_bridge = PathNode(under_bridge, Tile.GetTileDirectionDistance(under_bridge._tile, bridge._direction, _separation));
                            next_under_bridge._g_score = under_bridge._g_score + GEstimate(under_bridge, next_under_bridge);
                            next_under_bridge._bridge = true;
                            under_bridge = next_under_bridge;
                        }
                        /* Add the other side of the bridge to the path */
                        local other_side = PathNode(final_node, Tile.GetTileDirectionDistance(final_node._tile, bridge._direction, bridge._length));
                        other_side._g_score = under_bridge._g_score + GEstimate(under_bridge, other_side);
                        other_side._h_score = HEstimate(AIMap.GetTileX(other_side._tile), AIMap.GetTileY(other_side._tile));
                        /* Run along the path, setting the right G and H values and adding them to the closed list*/
                        local path = other_side;
                        while(path._previous != null)
                        {
                            path._previous._h_score = HEstimate(AIMap.GetTileX(path._tile), AIMap.GetTileY(path._tile));
                            path._g_score += break_node._g_score;
                            _closed_list.append(path);
                            if(path._previous != null) path = path._previous;
                            else break;
                        }
                        /* Connect the paths */
                        path._previous = break_node;
                        /* Add the final node to the open list */
                        _open_list.Insert(other_side, other_side._g_score + other_side._h_score);
                        break;
                    }
                    /* None of the bridges can be connected. Add all closed water tiles to this path finder's closed list and move on */
                    foreach(node in _water_closed_list) _closed_list.append(node);
                    continue;
                }
                continue;
            }
            
            /* Add current to the closed set */
            _closed_list.append(current);
            // AILog.Info("current: " + current.ToString());
            // AISign.BuildSign(current._tile, "current");
            // AILog.Info("Open list length: " + _open_list.Count());
        
            foreach(neighbour, val in Tile.GetNeighboursDistance(current._tile, _separation))
            {
                local direction = AbstractDirection.GetDirection(current._tile, neighbour);
                if(InList(_closed_list, neighbour, direction)) continue;
                
                local next = PathNode(current, neighbour);
                next._direction = AbstractDirection.GetDirection(current._tile, neighbour);
                next._bridge = false;
                
                /* Calculate this neighbour's g_score */
                local extra_g = GEstimate(current, next);
                if(extra_g == -1)
                {
                    _closed_list.append(next); //We're not allowed to build
                    continue;
                }
                else if(extra_g == -2)
                {
                    next._direction = AbstractDirection.N_S;
                    _closed_list.append(next);
                    next._direction = AbstractDirection.NE_SW;
                    _closed_list.append(next);
                    next._direction = AbstractDirection.E_W;
                    _closed_list.append(next);
                    next._direction = AbstractDirection.NW_SE;
                    _closed_list.append(next);
                    continue;
                }
                next._g_score = current._g_score + extra_g;
            
                /* Add it to the open list */
                next._h_score = HEstimate(AIMap.GetTileX(next._tile), AIMap.GetTileY(next._tile));
                if(!_open_list.Exists(next))
                {
                // AILog.Info("Adding " + Tile.ToString(next._tile) + " with g = " + next._g_score + " and h = " + next._h_score + " for f = " + (next._g_score + next._h_score));
                    _open_list.Insert(next, next._g_score + next._h_score);
                }
                //Because we're working with a tile-direction combination here, there are no other options to consider.
            }
        }
        
        //Whoops, there's no path
        return null;
    }
        
    
    /**
    *   Simply calculates the shortest possible distance by train from source to destination
    *
    *   @param  source_x    int     Source x location
    *   @param  source_y    int     Source y location
    *   @param  dest_x      int     Destination x location
    *   @param  dest_y      int     Destination y location
    *
    *   @return int The distance by train track
    */
    function HEstimate(source_x, source_y)
    {
        return Map.Distance(source_x, source_y, _dest_x, _dest_y);
    }
    
    
    /**
    *   Calculates the value that should be added to G from current to next.
    *
    *   @param  PathNode    current The node to calculate from
    *   @param  PathNode    next    The node to calculate to
    *
    *   @return int The value to be added to G, or -1 in case next is not a viable step
    */
    function GEstimate(current, next)
    {
        //Run this function in test mode
        local test_mode = AITestMode();
        local start_cost = _cost_empty_tile + abs(AITile.GetMaxHeight(current._tile) - AITile.GetMaxHeight(next._tile)) * _cost_height_difference;
        //First check for a normal flat buildable tile sequence     
        //Note: there might need to be a check for steep clopes here, and probably some other things. Leave it out for efficiency and let the low level
        //path finder worry about it
        if(AITile.IsBuildable(next._tile))
        {
            // local mode = AIExecMode();
            //AISign.BuildSign(next._tile, "easy");
            return start_cost;
        }
        //The new one might still be buildable. Just try
        switch(next._direction)
        {
            case AbstractDirection.NE_SW:
                if(AIRail.BuildRailTrack(next._tile, AIRail.RAILTRACK_NE_SW)) return start_cost;
            case AbstractDirection.NW_SE:       
                if(AIRail.BuildRailTrack(next._tile, AIRail.RAILTRACK_NW_SE)) return start_cost;
        }
        //Ok so we really can't build here. The only solution is a bridge. Bridges can't be built in every direction though. Use the direction we came from as a
        //check. This is not fail safe but nothing in this algorithm is.
        //Oh right first let's check the length of the bridge we might already be on
        local bridge_length = 0;
        local previous = current;
        while(previous._bridge)
        {
            bridge_length += _separation;
            previous = previous._previous;
        }
        if(bridge_length + _separation > _max_bridge_length) return -1;
        if(!AbstractDirection.AllowsBridge(next._direction) || (current._bridge && current._direction != next._direction)) return -1;
        //Ok so it might be possible to build a bridge. Only if the problem is water or a road or rail. Unless there's already another bridge and blahblah not
        //perfect as I said before. Let's also wisely ignore height differences.
        if(AITile.IsWaterTile(next._tile) || AIRoad.IsRoadTile(next._tile) || AIRail.IsRailTile(next._tile))
        {
            next._bridge = true;
            return start_cost + _cost_bridge * (bridge_length + _separation) / _separation;
        }
        //Exhausted options
        return -2;
    }
    
    
    /**
    *   This function starts by finding out how long the bridge across tile in direction would be. After that it recursively calls itself, walking off to the left and right and trying more bridges. In the end it might determine
    *   it would be best to just go around the lake. In that case, a tile is returned. All tried bridges are saved in _bridge_list and all water tiles that were tried are in _water_closed_list.
    *   
    *   @param tile         TileIndex   The tile across which a bridge should be built
    *   @param direction    Direction   The direction of the bridge
    *
    *   @return void if a bridge is suggested, a TileIndex in case going around the lake is suggested
    */
    function HandleLake(tile, direction)
    {
        /* If a bridge with a lower cost can never be found, return */
        if(_bridge_list.Count() > 0
                && Map.Distance(_initial_tile_x, _initial_tile_y, AIMap.GetTileX(_left_tile), AIMap.GetTileY(_left_tile)) > _bridge_list.Peek()._length * _bridge_vs_land
                && Map.Distance(_initial_tile_x, _initial_tile_y, AIMap.GetTileX(_right_tile), AIMap.GetTileY(_right_tile)) > _bridge_list.Peek()._length * _bridge_vs_land) return;
        /* Add this water tile and direction to the closed list */
        local node = PathNode(null, tile)
        AILog.Info("Adding " + Tile.ToString(tile) + ", " + Direction.ToAbstract(direction) + " to the water closed list");
        node._direction = Direction.ToAbstract(direction);
        _water_closed_list.append(node);
        /* From the initial tile on land, find out how long a bridge would need to be to get to the other side */
        local bridge = Bridge();
        bridge._start_tile = Tile.GetTileDirectionDistance(tile, Direction.Opposite(direction), _separation);
        bridge._length = TryBridge(bridge._start_tile, direction);
        AILog.Info("Bridge would need to be " + bridge._length + " long");
        /* If the bridge is possible, rate it and add it to the list */
        if(bridge._length != -1)
        {
            bridge._direction = Direction.Opposite(direction);
            bridge._end_tile = Tile.GetTileDirectionDistance(bridge._start_tile, bridge._direction, bridge._length)
            //Longer bridges gain cost, and bridges farther away from the initial tile gain cost
            local bridge_cost = bridge._length * 2 + Map.Distance(_initial_tile_x, _initial_tile_y, AIMap.GetTileX(tile), AIMap.GetTileY(tile)) + HEstimate(AIMap.GetTileX(bridge._end_tile), AIMap.GetTileY(bridge._end_tile));
            AILog.Info(bridge.ToString() + ", cost = " + bridge_cost);
            _bridge_list.Insert(bridge, bridge_cost);
        }
        /* Now walk along the neigbours of the water tile from the coast tile we came from, in order to find the next water tile and maybe try more bridges */
        direction = Direction.Opposite(direction);
        local counter = 0;
        while(true)
        {
            counter++;
            /* Determine which way we're walking along the coast from the original point, looking towards the lake */
            if(_going_left) direction = Direction.IncrementClockwise(direction);
            else direction = Direction.IncrementCounterClockwise(direction);
            local next;
            next = Tile.GetTileDirectionDistance(tile, direction, _separation);
            local mode = AIExecMode();
            AISign.BuildSign(next, "next");         
            if(AITile.IsWaterTile(next))
            {
                /* Found a water tile. Save it for the next step in the same direction (left/right) */
                if(_going_left)
                {
                    _left_tile = next;
                    //The direction from the last tile we checked while walking around clockwise, to this next tile
                    _left_direction = Direction.GetDirection(Tile.GetTileDirectionDistance(tile, Direction.IncrementCounterClockwise(direction), _separation), next);
                }
                else
                {
                    _right_tile = next;
                    //The direction from the last tile we checked while walking around counterclockwise, to this next tile
                    _right_direction = Direction.GetDirection(Tile.GetTileDirectionDistance(tile, Direction.IncrementClockwise(direction), _separation), next);
                }
                /* Recurse */
                _going_left = !_going_left;
                if(_going_left) return HandleLake(_left_tile, _left_direction);
                else return HandleLake(_right_tile, _right_direction);
            }
            else
            {
                /* Found another land tile. If it has a lower H value than the initial tile, we're now considered to have gone 'around the lake'. Return this tile */
                /* But only if it's buildable */
                if(!AITile.IsBuildable(next)) continue;
                /* But only if this has not been tried before */
                if(_initial_tile_h > HEstimate(AIMap.GetTileX(next), AIMap.GetTileY(next)) && !_around_list.HasItem(next))
                {
                    local mode = AIExecMode();
                    AISign.BuildSign(next, "Around");
                    _around_list.AddTile(next);
                    return next;
                }

                /* Can we build a bridge from here to the water tile in question? */
                if(counter % 2 == 0 && AITile.IsBuildable(next))
                {
                    /* Add this water tile and direction to the closed list */
                    node = PathNode(null, tile)
                    AILog.Info("Addink " + Tile.ToString(tile) + ", " + Direction.ToAbstract(direction) + " to the water closed list");
                    node._direction = Direction.ToAbstract(direction);
                    _water_closed_list.append(node);
                    
                    /* Create a new bridge */
                    bridge = Bridge();
                    bridge._start_tile = Tile.GetTileDirectionDistance(tile, direction, _separation);
                    bridge._length = TryBridge(bridge._start_tile, Direction.Opposite(direction));
                    /* If the bridge is possible, rate it and add it to the list */
                    if(bridge._length != -1)
                    {
                        bridge._direction = Direction.Opposite(direction);
                        bridge._end_tile = Tile.GetTileDirectionDistance(bridge._start_tile, bridge._direction, bridge._length)
                        //Longer bridges gain cost, and bridges farther away from the initial tile gain cost, and bridges in the wrong direction gain cost
                        local bridge_cost = bridge._length * _bridge_vs_land + Map.Distance(_initial_tile_x, _initial_tile_y, AIMap.GetTileX(next), AIMap.GetTileY(next)) + HEstimate(AIMap.GetTileX(bridge._end_tile), AIMap.GetTileY(bridge._end_tile));
                        AILog.Info(bridge.ToString() + ", cost = " + bridge_cost);
                        _bridge_list.Insert(bridge, bridge_cost);
                    }
                }
            }
        }
    }
    
    
    /**
    *   Checks how long a bridge crossing tile in direction would need to be
    *
    *   @param tile         TileIndex   The start tile of the bridge
    *   @param direction    Direction   The build direction of the bridge
    *
    *   @return int The length of the bridge if a bridge that long can be built, or -1 otherwise
    */
    function TryBridge(tile, direction)
    {
        if(!AITile.IsBuildable(tile)) return -1;
        local other_side;
        for(local length = _separation; length <= _max_bridge_length; length += _separation)
        {
            other_side = Tile.GetTileDirectionDistance(tile, direction, length);
            if(!AIMap.IsValidTile(other_side)) return -1;
            /* Look for a buildable tile */
            if(AITile.IsBuildable(other_side)) return length;
            /* Some tiles cannot be bridged. If we reach such a tile, a bridge cannot be built */
            if(!(AITile.IsWaterTile(other_side) || AIRoad.IsRoadTile(other_side) || AIRail.IsRailTile(other_side))) return -1;
        }
        
        return -1;
    }
    
    
    /**
    *   Returns the node a certain amount of nodes back up the path
    *
    *   @param  from        PathNode    The node to walk back from
    *   @param  distance    int         The distance to walk back
    *
    *   @return The node distance back from from, or the first node of the path in case it's less than distance away
    */
    function StepBack(from, distance)
    {
        local result = from;
        while(result._previous != null && distance > 0)
        {
            result = result._previous;
            distance--;
        }
        return result;
    }
    
    
    /**
    *   Walks through the list backwards looking for this tile-direction combination
    *
    *   @param  list        array
    *   @param  tile        TileIndex
    *   @param  direction   AbstractDirection flag
    *
    *   @return boolean True if this tile-direction combo is in the list, false otherwise
    */
    function InList(list, tile, direction)
    {
        //Go rear to front because usually if it's in there it will be in the second half
        for(local i = list.len()-1; i >= 0; i--)
        {
            if(list[i]._tile == tile && list[i]._direction == direction) return true;
        }
        return false;
    }
}