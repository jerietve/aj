class Bridge
{
    _start_tile = null;
    _end_tile   = null;
    _direction  = null;
    _length     = null;
    
    constructor()
    {
    }
    
    
    /**
    *   Returns the TTD that would be the end if it were not a bridge. For finding the next options after this bridge
    */
    function GetEndTTD()
    {
        if(_end_tile == null || _direction == null || _length == null) return null;
        
        local track;
        if(_direction == Direction.NORTHWEST || _direction == Direction.SOUTHEAST) track = AIRail.RAILTRACK_NW_SE;
        else track = AIRail.RAILTRACK_NE_SW;
        return TTD(track, _end_tile, _direction);
    }
    
    
    function Equals(other_bridge)
    {
        return this._start_tile == other_bridge._start_tile && this._direction = other_bridge._direction && this._length == other_bridge._length;
    }
    
    
    function ToString()
    {
        return "Bridge from " + Tile.ToString(_start_tile) + " to " + Tile.ToString(_end_tile) + ", length " + _length + ", direction " + Direction.ToString(_direction);
    }
}