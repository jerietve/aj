// Time test:
local before_tick = AIController.GetTick();
local before_ops = AIController.GetTick() * 10000 - AIController.GetOpsTillSuspend();
local after_ops = AIController.GetTick() * 10000 - AIController.GetOpsTillSuspend();
local after_tick = AIController.GetTick();
AILog.Info("That took " + (after_ops - before_ops - 9) + " ops, also " + (after_tick - before_tick) + " ticks");