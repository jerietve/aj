class RailBuilder
{
    _search_light_dist = 40;
    _close_enough_dist = 4;
    _build_dist        = 8;
    _bridge_speed      = 0;
    tp = null;
    rpf = null;
    
    
    constructor()
    {
        tp = ::ai_instance.tp;
        rpf = RailPathFinder();
    }
    
    
    /**
    *   Tries to build a railroad of somewhere along the abstract path provided. Building will commence immediately. If
    *   for whatever reason a path to the final destination cannot be found, all tracks are deleted.
    *   The path is built from the path node that is abstract_path, from to to from. The from and to part are not built.
    *
    *   @param  from_part_loc   PartLocation    The PartLocation from which to build a path.
    *   @param  to_part_loc     PartLocation    The PartLocation to which to build a path.
    *   @param  PathNode        abstract_path   An abstract path built by the AbstractPathFinder
    *   @param  int             bridge_speed    The preferred minimum velocity allowed on bridges
    *
    *   @return boolean true if building was successful, false in case everything built has been deleted
    */
    function BuildTrackAlongAbstractPath(from_part_loc, to_part_loc, abstract_path, bridge_speed)
    {
        _bridge_speed = bridge_speed;
        /* Copy from_part_loc, because we're going to mess this one up */
        local current_part_loc = PartLocation(to_part_loc.part_id, to_part_loc.tile);
        
        /* First convert the path into one with a little more information */
        abstract_path.Finalize();
        local current = abstract_path;
        /* Fill the path buffer */
        local path;
        local dist_to_go = _search_light_dist;
        while(current._dist_to_start >= _build_dist && dist_to_go > 0)
        {
            //Move current a small distance away, but don't stop half way a bridge
            local bridge_test = [true];
            local next = current.GetNodeAtLeastDistAlongPath(_build_dist, bridge_test);
            dist_to_go -= Map.DistanceTiles(current._tile, next._tile);
            current = next;
            path = ExtendBuffer(current, current_part_loc, path, bridge_test[0]);
            if(path == null) return false;
            local finish = path.GetFinish();
            if(finish._part_loc.part_id == TrackParts.BRIDGE_SEGMENT) current_part_loc = finish._part_data.end_part_loc;
            else current_part_loc = finish._part_loc;
        }
        /* Keep building and refilling the buffer till the buffer reaches the end */
        while(current._dist_to_start >= _build_dist)
        {
            /* Build a little bit */
            path = BuildPathTillDistFromDest(path, _search_light_dist - _build_dist, current._tile);
            if(path == null)
            {
                //TODO don't just delete everything because somebody is sabotaging us. Step back a little and retry.
                DeleteBuiltTrack();
                return false;
            }
            /* Buffer a little bit */
            local bridge_test = [true];
            current = current.GetNodeAtLeastDistAlongPath(_build_dist, bridge_test);
            path = ExtendBuffer(current, current_part_loc, path, bridge_test[0]);
            if(path == null)
            {
                DeleteBuiltTrack();
                return false;
            }
            local finish = path.GetFinish();
            if(finish._part_loc.part_id == TrackParts.BRIDGE_SEGMENT) current_part_loc = finish._part_data.end_part_loc;
            else current_part_loc = finish._part_loc;
        }
        /* Find the path to the actual precise end, and build everything */
        local extra_path = rpf.FindPath(current_part_loc, from_part_loc, 0, true, path == null);//Very short path, no need to worry about long bridges (also no way to know)
        path = path.ConcatenateRemoveLast(extra_path);
        if(!BuildPath(path)) DeleteBuiltTrack();
    }
    
    
    /**
    *   Builds track along path until it reaches a tile which is no more than dist tiles away from dest. Path is
    *   advanced to the node that has this tile and it's _previous it set to null.
    *
    *   @param  path    RailPathNode    The RailPathNode pointing to the beginning of the path
    *   @param  dist    int             Distance from dest at which to stop building
    *   @param  dest    AITileIndex     Destination tile
    *
    *   @return RailPathNode    The node of the last piece that was built, or null in case building could not be completed or a null path was provided
    *           boolean         True in case the whole path was built           
    */
    function BuildPathTillDistFromDest(path, dist, dest)
    {
        local exec = AIExecMode();
        local current_part_loc = null;
        local current_part_number = 0;
        while(path != null)
        {
            //TODO implement NW_NE -> SW_SE and the other way around and forward and backward and the other diagonal :)
            if(TrackParts.IsStraight(path._part_loc.part_id) && current_part_loc != null && path._part_loc.part_id == current_part_loc.part_id)
            {
                /* Same part as last time, add it to the queue */
                current_part_number++;
            }
            else
            {
                if(current_part_number == 1)
                {
                    if(!BuildPartLoc(current_part_loc)) return null;
                    current_part_number = 0;
                    current_part_loc = null;
                }
                else if(current_part_number > 1)
                {
                    /* Different part than last time. Build the queue and reset it */
                    if(!BuildPartLocTimes(current_part_loc, current_part_number)) return null;
                    current_part_number = 0;
                    current_part_loc = null;
                }
                //See if we have to build a bridge. Bridges are not queued
                if(path._part_loc.part_id == TrackParts.BRIDGE_SEGMENT)
                {
                    /* Build the whole segment, which can include multiple bridges and pieces of track. Build it track by
                        track: first single, then double (if necessary), then triple etc. */
                    local track_descriptions = path._part_data.track_descriptions;
                    local current_tile;
                    local part = tp.parts[path._part_data.start_part_loc.part_id];
                    local base_track = part.sections[0].track;
                    for(local i = 0; i < part.sections.len(); i++)
                    {
                        current_tile = path._part_loc.tile + part.sections[i].offset;
                        for(local k = 0; k < track_descriptions[i].len(); k++)
                        {
                            if(track_descriptions[i][k] == 1)
                            {
                                if(!AIRail.BuildRailTrack(current_tile, base_track)) return null;
                                current_tile += part.next_tile;
                            }
                            else
                            {
                                local bridge_list = AIBridgeList_Length(track_descriptions[i][k]);
                                //Choose a bridge
                                local best_bridge = bridge_list.Begin();
                                foreach(bridge in bridge_list)
                                {
                                    if(AIBridge.GetMaxSpeed(bridge) > AIBridge.GetMaxSpeed(best_bridge) && AIBridge.GetMaxSpeed(best_bridge) < _bridge_speed) best_bridge = bridge;
                                    if(AIBridge.GetMaxSpeed(bridge) < AIBridge.GetMaxSpeed(best_bridge) && AIBridge.GetMaxSpeed(bridge) > _bridge_speed) best_bridge = bridge;
                                }
                                if(!AIBridge.BuildBridge(AIVehicle.VT_RAIL, best_bridge, current_tile, Tile.GetTileDirectionDistance(current_tile, part.direction, track_descriptions[i][k]))) return null;
                                current_tile = Tile.GetTileDirectionDistance(current_tile, part.direction, track_descriptions[i][k] + 1);
                            }
                        }
                    }
                }
                else
                {
                    /* New part; start a new queue */
                    current_part_loc = path._part_loc;
                    current_part_number = 1;
                }
            }
            path = path._next;
            if(path == null) return true;
            path._previous = null;
            //TODO possibility of building long bridge segment over and too far away from dest
            if(Map.DistanceTiles(path._part_loc.tile, dest) < dist)
            {
                /* Build whatever's in the queue and return */
                if(current_part_number == 1)
                {
                    if(!BuildPartLoc(current_part_loc)) return null;
                }
                else if(current_part_number > 1)
                {
                    if(!BuildPartLocTimes(current_part_loc, current_part_number)) return null;
                }
                return path;
            }
        }
        return null;
    }
    
    
    /**
    *   Builds the particular part at its location
    *
    *   @param  part_loc    PartLocation    What's to be built
    *
    *   @return boolean True if the complete part was built, false if some section failed to be built
    */
    function BuildPartLoc(part_loc)
    {
        local exec = AIExecMode();
        local part = tp.parts[part_loc.part_id];
        local sections = part.sections;
        foreach(section in sections)
        {
            if(!AIRail.BuildRailTrack(part_loc.tile + section.offset, section.track)) return false;
        }
        return true;
    }
    
    
    /**
    *   Builds the particular part a number of times in a row starting at its location
    *
    *   @param  part_loc    PartLocation    What's to be built. Must be a straight part
    *   @param  times       int             How many times it's to be built
    *
    *   @return boolean True if the complete series was built, false if a track failed
    */
    function BuildPartLocTimes(part_loc, times)
    {
        if(part_loc == null || times < 1) return true;
        local part = tp.parts[part_loc.part_id];
        /* Find out the tile connected to the part's base tile where the right track starts */
        local from = Edge.GetNeighbour(part_loc.tile, Edge.GetFromEdgeTrackDirection(part.sections[0].track, part.direction));
        local to = part_loc.tile;
        for(local i = 1; i <= times; i++)
        {
            to += part.next_tile;
        }
        for(local i = 0; i < part.track_start_tiles.len(); i++)
        {
            if(!AIRail.BuildRail(from + part.track_start_tiles[i], part_loc.tile + part.track_start_tiles[i], to + part.track_start_tiles[i])) return false;
        }
        return true;
    }
        
    
    /**
    *   Adds a path from the current path end to _close_enough_dist away from current to path and returns path. In case
    *   a path to current can't be found, finding a path to a successor of current will be tried.
    */
    function ExtendBuffer(current, from_part_loc, path, cheap_bridges)
    {
        //Create a bogus PartLocation
        local temp_part_loc = PartLocation(from_part_loc.part_id, current._tile);
        local extra_path = rpf.FindPath(from_part_loc, temp_part_loc, _close_enough_dist, cheap_bridges, path == null);
        if(extra_path == null)
        {
            /*  A path from the end of the last one to current can't be found. We're going to step back a little and try
                again. If that doesn't work either, we're gonna look for a path not to current but to a bit further
                along the abstract path. If that doesn't work, keep on moving farther away from the original path we
                needed to find by moving current ahead and from_array back, until we hit the beginning of path. If that
                happens, or if it simply takes too long, give up and return false. Do mind that the 'beginning of the
                path' is where the builder currently is, it's not the actual beginning of the track. This could be
                extended by deleting a piece of built track, but setting the building distance variables is probably a
                better way because using the detailed pathfinder for finding too long a path will take a lot of time. */
            local start_date = AIDate.GetCurrentDate();
            local move_current = false;
            while(extra_path == null)
            {
                /* Let's say we have five days */
                // if(AIDate.GetCurrentDate() > start_date + 5)
                // {
                    // AILog.Info("Ran out of time");
                    // return false;
                // }
                if(move_current)
                {
                    /* Move current forward along the abstract path */
                    current = current.GetNodeAtLeastDistAlongPath(_build_dist, true);
                    if(current == null) return null;
                    //Update the temporary PartLocation
                    temp_part_loc = PartLocation(from_part_loc.part_id, current._tile);
                    move_current = false;
                }
                else
                {
                    /* Move back along path to a point at least _build_dist away from where it is now */
                    //Get to the end
                    if(path == null) return null;
                    path = path.GetFinish();
                    //Move back
                    while(true)
                    {
                        //Reached the start? Fail
                        //The reason previous' previous is checked is because the first part can't be changed anyway
                        if(path._previous == null || path._previous._previous == null)
                        {
                            AILog.Info("Hit the start of the path, at " + Tile.ToString(path._part_loc.tile));
                            return null;
                        }
                        path = path._previous;
                        //See if we've moved back enough
                        if(Map.DistanceTiles(path._part_loc.tile, from_part_loc.tile) > _build_dist) break;
                    }
                    //Delete the tail we just walked back over
                    path._next = null;
                    if(path._part_loc.part_id == TrackParts.BRIDGE_SEGMENT) from_part_loc = path._part_data.end_part_loc;
                    else from_part_loc = path._part_loc;
                    move_current = true;
                }
                extra_path = rpf.FindPath(from_part_loc, temp_part_loc, _close_enough_dist, cheap_bridges, false);
            }
        }
        else if(extra_path.NumberOfNodes() * 5 < extra_path.GetFinish()._g_score && path != null)
        {
            /* This path is very expensive for its length. Might be that we were forced to start a bridge which is not
                strictly necessary. Move back once and try again */
            /* Move back along path to a point at least _build_dist away from where it is now */
            path = path.GetFinish();
            local fail = false;
            while(true)
            {
                //Reached the start? Fail
                //The reason previous' previous is checked is because the first part can't be changed anyway
                if(path._previous == null || path._previous._previous == null)
                {
                    fail = true;
                    break;
                }
                path = path._previous;
                //See if we've moved back enough
                if(Map.DistanceTiles(path._part_loc.tile, from_part_loc.tile) > _build_dist) break;
            }
            if(!fail)
            {
                //Delete the tail we just walked back over
                path._next = null;
                if(path._part_loc.part_id == TrackParts.BRIDGE_SEGMENT) from_part_loc = path._part_data.end_part_loc;
                else from_part_loc = path._part_loc;
                extra_path = rpf.FindPath(from_part_loc, temp_part_loc, _close_enough_dist, cheap_bridges, false);
            }
        }
        //Add the path to what we already had
        if(path == null) path = extra_path;
        else path = path.ConcatenateRemoveLast(extra_path);
        path = path.GetStart();
        
        return path;
    }
    
    
    /**
    *   Builds track along path
    *
    *   @param  path    RailPathNode    The RailPathNode pointing to the beginning of the path
    *
    *   @return boolean True if the complete path was built, false if an obstacle was hit. The track is built until the
    *                   obstacle
    */
    function BuildPath(path)
    {
        if(BuildPathTillDistFromDest(path, AIMap.GetTileIndex(0,0), 0)) return true;
        else return false;
        // //We're going to really build something
        // AIRail.SetCurrentRailType(0);
        // local exec = AIExecMode();
        
        // while(path != null)
        // {
            // //See if we have to build a bridge
            // if(path._part == TrackParts.BRIDGE_SEGMENT)
            // {
                // //Get the bridge list
                // local bridge_list = AIBridgeList_Length(path._special_part_data.length.tointeger())
                // //Choose a bridge
                // local best_bridge = bridge_list.Begin();
                // foreach(bridge, value in bridge_list)
                // {
                    // if(bridge.GetMaxSpeed() > best_bridge.GetMaxSpeed() && best_bridge.GetMaxSpeed() < _bridge_speed) best_bridge = bridge;
                    // if(bridge.GetMaxSpeed() < best_bridge.GetMaxSpeed() && bridge.GetMaxSpeed() > _bridge_speed) best_bridge = bridge;
                // }
                // AILog.Info("Building bridge from start tile " + Tile.ToString(path._bridge._start_tile) + " to end tile " + Tile.ToString(path._bridge._end_tile));
                // if(!AIBridge.BuildBridge(AIVehicle.VT_RAIL, best_bridge, path._bridge._start_tile, path._bridge._end_tile))
                // {
                    // AILog.Warning("Could not build bridge from " + Tile.ToString(path._bridge._start_tile) + " to " + Tile.ToString(path._bridge._end_tile) + "!: " + AIError.GetLastErrorString());
                    // return false;
                // }
            // }
            // else if(!AIRail.BuildRailTrack(path._TTD._tile, path._TTD._track))
            // {
                // AILog.Warning("Could not build track " + path._TTD._track + " on " + Tile.ToString(path._TTD._tile) + "!: " + AIError.GetLastErrorString());
                // return false;
            // }
            // path = path._next;
        // }
        
        // return true;
    }
}