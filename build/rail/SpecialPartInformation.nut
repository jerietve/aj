/**
*   This class contains all information included in a RailPathNode for the RailBuilder to be able to build a section
*   with one or more bridges
*/
class BridgeInformation
{
    end_part_loc   = null;  //Used for finding which parts could be next after this section
    start_part_loc = null;
    /*  An array of arrays describing the tracks. There's one array for every track (a double track part having two). A 
        1 in the array means a single railtrack, any other number means a bridge of that length. They appear in order
        from the start partloc to the end partloc */
    track_descriptions = null;  
}


class TunnelInformation
{

}