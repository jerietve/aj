class TrackPartBuilder
{
    /**
     * Finds out whether if money were not an issue a track part could end up being owned by us in that location.
     *
     * @param   part_loc    PartLocation    The part location to try to build.
     *
     * @return  bool    True iff each section of the track part could be built or already exists.
     */
    function TryBuildIgnoreMoneyAndExisting(part_loc)
    {
        local test_mode = AITestMode();
        
        if(!TrackParts.IsSingleTrack(part_loc.part_id))
        {
            throw "Unimplemented";
        }
        
        local part = tp.parts[part_loc.part_id];
        local sections = part.sections;
        local section = sections[0];
        local tile = section.offset + part_loc.tile;
        
        if(AIRail.BuildRailTrack(tile, section.track)) return true;
        
        local error = AIError.GetLastError();
        if(error == AIError.ERR_NOT_ENOUGH_CASH || error == AIError.ERR_ALREADY_BUILT) return true;
    }
}