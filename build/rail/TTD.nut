/* TTD stands for Track, Tile, Direction. It's simply a combination class for those three things. But I thought TTD was
    kind of a cool name */
class TTD
{
    _track     = null; //The AIRail.RailTrack
    _tile      = null; //The AITile
    _direction = null; //The Direction.Direction. When building, this should be the building direction, not the driving direction
    
    constructor(track, tile, direction)
    {
        this._track = track;
        this._tile = tile;
        this._direction = direction;
    }
    
    
    /**
    *   Returns the TTDs that could be the next after this. It does not do any check to see whether or not they can be
    *   built. The only check that is done is for the validity of tiles.
    *
    *   @return array   The TTDs that could be next. In case the next tile is not valid, an empty array is returned.
    */
    function GetNextTTDs()
    {
        /*  The next possible TTDs depend on the edge of the tile where this TTDs track ends, and on the track itself
            because the author will not allow trains to make 90 degree turns, ever. There are four edges.
            However, because the edge is not stored in the TTD explicitly, we'll have to find it first. All four options
            are the result of one of three possible direction-track combinations and will result in two or three new
            TTDs that can be built next. Tracks along an axis have three options, others two.
        */
        local edge;
        edge = Util.GetEdgeTrackDirection(_track, _direction);
        //Get the neighbouring tile (there's only one)
        local neighbour_tile = Edge.GetNeighbour(_tile, edge);
        if(!AIMap.IsValidTile(neighbour_tile)) return [];
        //Get the edge we're coming from in that tile
        edge = Edge.Opposite(edge);
        //Create the TTDs to return them
        local result = [];
        //Tracks along an axis are easy
        if(_track == AIRail.RAILTRACK_NE_SW || _track == AIRail.RAILTRACK_NW_SE)
        {
            //The edge we're switching on is the edge we're coming IN through in the next tile
            switch(edge)
            {
                case Edge.NORTHEAST:
                    result.append(TTD(AIRail.RAILTRACK_NE_SW, neighbour_tile, Direction.SOUTHWEST));
                    result.append(TTD(AIRail.RAILTRACK_NE_SE, neighbour_tile, Direction.SOUTH));
                    result.append(TTD(AIRail.RAILTRACK_NW_NE, neighbour_tile, Direction.WEST));
                    break;
                case Edge.SOUTHEAST:
                    result.append(TTD(AIRail.RAILTRACK_NW_SE, neighbour_tile, Direction.NORTHWEST));
                    result.append(TTD(AIRail.RAILTRACK_SW_SE, neighbour_tile, Direction.WEST));
                    result.append(TTD(AIRail.RAILTRACK_NE_SE, neighbour_tile, Direction.NORTH));
                    break;
                case Edge.SOUTHWEST:
                    result.append(TTD(AIRail.RAILTRACK_NE_SW, neighbour_tile, Direction.NORTHEAST));
                    result.append(TTD(AIRail.RAILTRACK_SW_SE, neighbour_tile, Direction.EAST));
                    result.append(TTD(AIRail.RAILTRACK_NW_SW, neighbour_tile, Direction.NORTH));
                    break;
                case Edge.NORTHWEST:
                    result.append(TTD(AIRail.RAILTRACK_NW_SE, neighbour_tile, Direction.SOUTHEAST));
                    result.append(TTD(AIRail.RAILTRACK_NW_SW, neighbour_tile, Direction.SOUTH));
                    result.append(TTD(AIRail.RAILTRACK_NW_NE, neighbour_tile, Direction.EAST));
                    break;
                default:
                    throw("Invalid Edge type in TTD.GetNextTTDs: " + edge);
            }
        }
        //Diagonal tracks are a bit more difficult, because they influence the options
        else
        {
            //The edge we're switching on is the edge we're coming IN through in the next tile
            switch(edge)
            {
                case Edge.NORTHEAST:
                    if(_track == AIRail.RAILTRACK_SW_SE)
                    {
                        result.append(TTD(AIRail.RAILTRACK_NE_SW, neighbour_tile, Direction.SOUTHWEST));
                        result.append(TTD(AIRail.RAILTRACK_NW_NE, neighbour_tile, Direction.WEST));
                        break;
                    }
                    else
                    //NW_SW
                    {
                        result.append(TTD(AIRail.RAILTRACK_NE_SW, neighbour_tile, Direction.SOUTHWEST));
                        result.append(TTD(AIRail.RAILTRACK_NE_SE, neighbour_tile, Direction.SOUTH));
                        break;
                    }
                case Edge.SOUTHEAST:
                    if(_track == AIRail.RAILTRACK_NW_NE)
                    {
                        result.append(TTD(AIRail.RAILTRACK_NW_SE, neighbour_tile, Direction.NORTHWEST));
                        result.append(TTD(AIRail.RAILTRACK_SW_SE, neighbour_tile, Direction.WEST));
                        break;
                    }
                    else
                    //NW_SW
                    {
                        result.append(TTD(AIRail.RAILTRACK_NW_SE, neighbour_tile, Direction.NORTHWEST));
                        result.append(TTD(AIRail.RAILTRACK_NE_SE, neighbour_tile, Direction.NORTH));
                        break;
                    }
                case Edge.SOUTHWEST:
                    if(_track == AIRail.RAILTRACK_NW_NE)
                    {
                        result.append(TTD(AIRail.RAILTRACK_NE_SW, neighbour_tile, Direction.NORTHEAST));
                        result.append(TTD(AIRail.RAILTRACK_SW_SE, neighbour_tile, Direction.EAST));
                        break;
                    }
                    else
                    //NE_SE
                    {
                        result.append(TTD(AIRail.RAILTRACK_NE_SW, neighbour_tile, Direction.NORTHEAST));
                        result.append(TTD(AIRail.RAILTRACK_NW_SW, neighbour_tile, Direction.NORTH));
                        break;
                    }
                case Edge.NORTHWEST:
                    if(_track == AIRail.RAILTRACK_SW_SE)
                    {
                        result.append(TTD(AIRail.RAILTRACK_NW_SE, neighbour_tile, Direction.SOUTHEAST));
                        result.append(TTD(AIRail.RAILTRACK_NW_NE, neighbour_tile, Direction.EAST));
                        break;
                    }
                    else
                    //NE_SE
                    {
                        result.append(TTD(AIRail.RAILTRACK_NW_SE, neighbour_tile, Direction.SOUTHEAST));
                        result.append(TTD(AIRail.RAILTRACK_NW_SW, neighbour_tile, Direction.SOUTH));
                        break;
                    }   
                default:
                    throw("Invalid Edge type in TTD.GetNextTTDs: " + edge);             
            }
        }
        return result;
    }
    
    
    /**
    *   Returns true if the other TTD has exactly the same values (all properties are ==d)
    */
    function Equals(other_TTD)
    {
        return this._track == other_TTD._track && this._tile == other_TTD._tile && this._direction == other_TTD._direction;
    }
    
    
    function ToString()
    {
        return "TTD: Track = " + _track + ", tile = " + Tile.ToString(_tile) + ", direction = " + Direction.direction_names[_direction];
    }
}