/* All rail tracks are somehow implemented by Part.  */
class Part
{
    sections = null;        //This is an array of sections (track, offset combinations) that make up this part
    points = null;          //This is an array of points (north points of tiles) that affect this part
}


/* A conventional part is a part that is just a piece of track without junctions or depots or stations or whatever */
class ConventionalPart extends Part
{
    next_tile = null;           //This will be the base tile for the next part
    next_part_ids = null;       //This is an array of possible parts to put behind this one
    direction = null;           //The direction
    length = null;              //The distance from the base tile to the next tile
    track_start_tiles = null;   //The relative positions of tiles containing the first piece of rails for each track
    nr_tracks = null;           //The number of tracks (2 for double track for instance)
}   


/* A PartLocation is a combination of a part and tile, so it is like an actual buildable instance of a part */
class PartLocation
{
    part_id = null; //The ID of the part
    tile = null;    //The ID of the tile
    
    constructor(part_id, tile)
    {
        this.part_id = part_id;
        this.tile = tile;
    }
    
    
    /**
    *   Returns the PartLocations that can come after this one. No AITile validation is done
    */
    function NextPartLocations()
    {
        local tp = ::ai_instance.tp;
        local part = tp.parts[part_id];
        local next_part_ids = part.next_part_ids;
        local next_part_locations = array(next_part_ids.len());
        for(local i = 0; i < next_part_ids.len(); i++)
        {
            next_part_locations[i] = PartLocation(next_part_ids[i], tile + part.next_tile);
        }
        
        return next_part_locations;
    }
    
    
    function PartId()
    {
        return part_id;
    }
    
    
    function Equals(other_part_loc)
    {
        return this.part_id == other_part_loc.part_id && this.tile == other_part_loc.tile;
    }
    
    
    function ToString()
    {
        return "PartLocation: " + ::ai_instance.tp.part_names[part_id] + ", " + Tile.ToString(tile);
    }
}


/* A section is a combination of an offset (a relative AITile location) and an AIRail.RAILTRACK */
class Section
{
    offset = null;
    track = null;
    
    constructor(offset, track)
    {
        this.offset = offset;
        this.track = track;
    }
}


/* A point is a corner of a tile, including the tile whose north corner we're referring to and the height of this corner */
class Point
{
    tile = null;
    height = null;
    
    constructor(tile, height)
    {
        this.tile = tile;
        this.height = height;
    }
}


class TrackParts
{
    /* Every trackpart is considered in two directions for speeding up the checking of next tracks */
    /* Single tracks */
    /* Conventional single tracks are named by the edges of tiles they connect. */
    static NW_SE = 0;
    static SE_NW = 1;
    static SW_NE = 2;
    static NE_SW = 3;
    static SW_SE = 4;
    static SE_SW = 5;
    static NW_NE = 6;
    static NE_NW = 7;
    static NW_SW = 8;
    static SW_NW = 9;
    static NE_SE = 10;
    static SE_NE = 11;
    
    /* Double tracks */
    /* Double line tracks are named by the direction they came from and the direction they're headed */
    static D_NW_SE = 100;
    static D_SE_NW = 101;
    static D_SW_NE = 102;
    static D_NE_SW = 103;
    /* Double diagonal tracks are named by the direction they came from and are headed and which track starts first,
        left or right */
    static D_N_S_RF = 104;
    static D_S_N_RF = 105;
    static D_N_S_LF = 106;
    static D_S_N_LF = 107;
    static D_W_E_RF = 108;
    static D_E_W_RF = 109;
    static D_W_E_LF = 110;
    static D_E_W_LF = 111;
    /* Double corners are named by the edge they come from and go to */
    static D_NW_NE = 112;
    static D_NE_NW = 113;
    static D_SW_SE = 114;
    static D_SE_SW = 115;
    static D_NE_SE = 116;
    static D_SE_NE = 117;
    static D_NW_SW = 118;
    static D_SW_NW = 119;
    
    /* Special parts */
    static BRIDGE_SEGMENT = 42;
    static TUNNEL_SEGMENT = 43;
    
    static HIGHEST_PART_NUMBER = 119;
    
    part_names = null;
    
    parts = null;
    
    constructor()
    {
        part_names = InitializePartNames();
        Initialize();
    }
    
    static function IsConventionalPart(part_id);
    static function IsLine(part_id);
    static function IsStraight(part_id);
    static function IsDiagonal(part_id);
    static function IsSingleTrack(part_id);
    static function IsDoubleTrack(part_id);
    
    /* Debug */
    function BuildAllParts();
    function BuildAllPartsWithNexts();
    
    /* Private */
    function Initialize();
}


/**
*   Cycles through all parts and puts them in the parts array. All parts will have whatever they need (sections, points,
*   etc) set.
*/
function TrackParts::Initialize()
{
    /* Instantiate the parts array and all parts*/
    local i, part;
    parts = array(HIGHEST_PART_NUMBER + 1);
    for(i = 0; i <= HIGHEST_PART_NUMBER; i++)
    {
        if(IsConventionalPart(i))
        {
            parts[i] = ConventionalPart();
        }
    }
    
    /* Set all parts their values. The meaning of the values is explained in the respective Part classes */
    
    /**** Single parts ****/
    
    /*** Conventional parts ***/
    
    /** Line parts **/
    
    /* Complete NW_SE */
    part = parts[NW_SE];
    part.next_tile = AIMap.GetTileIndex(0,1);
    part.length = 1;
    part.direction = Direction.SOUTHEAST;
    part.sections = [Section(AIMap.GetTileIndex(0,0), AIRail.RAILTRACK_NW_SE)];
    part.next_part_ids = [NW_NE, NW_SE, NW_SW];
    part.points = array(4);
    part.points[0] = AIMap.GetTileIndex(0,0);
    part.points[1] = AIMap.GetTileIndex(1,0);
    part.points[2] = AIMap.GetTileIndex(0,1);
    part.points[3] = AIMap.GetTileIndex(1,1);
    part.track_start_tiles = [AIMap.GetTileIndex(0,0)];
    part.nr_tracks = 1;
    
    /* Complete SE_NW */
    part = parts[SE_NW];
    part.next_tile = AIMap.GetTileIndex(0,-1);
    part.length = 1;
    part.direction = Direction.NORTHWEST;
    part.sections = [Section(AIMap.GetTileIndex(0,0), AIRail.RAILTRACK_NW_SE)];
    part.next_part_ids = [SE_NW, SE_SW, SE_NE];
    part.points = array(4);
    part.points[0] = AIMap.GetTileIndex(0,0);
    part.points[1] = AIMap.GetTileIndex(1,0);
    part.points[2] = AIMap.GetTileIndex(0,1);
    part.points[3] = AIMap.GetTileIndex(1,1);
    part.track_start_tiles = [AIMap.GetTileIndex(0,0)];
    part.nr_tracks = 1;
    
    /* Complete SW_NE */
    part = parts[SW_NE];
    part.next_tile = AIMap.GetTileIndex(-1,0);
    part.length = 1;
    part.direction = Direction.NORTHEAST;
    part.sections = [Section(AIMap.GetTileIndex(0,0), AIRail.RAILTRACK_NE_SW)];
    part.next_part_ids = [SW_NE, SW_SE, SW_NW];
    part.points = array(4);
    part.points[0] = AIMap.GetTileIndex(0,0);
    part.points[1] = AIMap.GetTileIndex(1,0);
    part.points[2] = AIMap.GetTileIndex(0,1);
    part.points[3] = AIMap.GetTileIndex(1,1);
    part.track_start_tiles = [AIMap.GetTileIndex(0,0)];
    part.nr_tracks = 1;
    
    /* Complete NE_SW */
    part = parts[NE_SW];
    part.next_tile = AIMap.GetTileIndex(1,0);
    part.length = 1;
    part.direction = Direction.SOUTHWEST;
    part.sections = [Section(AIMap.GetTileIndex(0,0), AIRail.RAILTRACK_NE_SW)];
    part.next_part_ids = [NE_SW, NE_NW, NE_SE];
    part.points = array(4);
    part.points[0] = AIMap.GetTileIndex(0,0);
    part.points[1] = AIMap.GetTileIndex(1,0);
    part.points[2] = AIMap.GetTileIndex(0,1);
    part.points[3] = AIMap.GetTileIndex(1,1);
    part.track_start_tiles = [AIMap.GetTileIndex(0,0)];
    part.nr_tracks = 1;
    
    /** Diagonal parts **/
    
    /* Complete SW_SE */
    part = parts[SW_SE];
    part.next_tile = AIMap.GetTileIndex(0,1);
    part.length = sqrt(2)/2;
    part.direction = Direction.EAST;
    part.sections = [Section(AIMap.GetTileIndex(0,0), AIRail.RAILTRACK_SW_SE)];
    part.next_part_ids = [NW_NE, NW_SE];
    part.points = array(3);
    part.points[0] = AIMap.GetTileIndex(1,0);
    part.points[1] = AIMap.GetTileIndex(0,1);
    part.points[2] = AIMap.GetTileIndex(1,1);
    part.track_start_tiles = [AIMap.GetTileIndex(0,0)];
    part.nr_tracks = 1;
    
    /* Complete SE_SW */
    part = parts[SE_SW];
    part.next_tile = AIMap.GetTileIndex(1,0);
    part.length = sqrt(2)/2;
    part.direction = Direction.WEST;
    part.sections = [Section(AIMap.GetTileIndex(0,0), AIRail.RAILTRACK_SW_SE)];
    part.next_part_ids = [NE_NW, NE_SW];
    part.points = array(3);
    part.points[0] = AIMap.GetTileIndex(1,0);
    part.points[1] = AIMap.GetTileIndex(0,1);
    part.points[2] = AIMap.GetTileIndex(1,1);
    part.track_start_tiles = [AIMap.GetTileIndex(0,0)];
    part.nr_tracks = 1;
    
    /* Complate NW_NE */
    part = parts[NW_NE];
    part.next_tile = AIMap.GetTileIndex(-1,0);
    part.length = sqrt(2)/2;
    part.direction = Direction.EAST;
    part.sections = [Section(AIMap.GetTileIndex(0,0), AIRail.RAILTRACK_NW_NE)];
    part.next_part_ids = [SW_SE, SW_NE];
    part.points = array(3);
    part.points[0] = AIMap.GetTileIndex(0,0);
    part.points[1] = AIMap.GetTileIndex(1,0);
    part.points[2] = AIMap.GetTileIndex(0,1);
    part.track_start_tiles = [AIMap.GetTileIndex(0,0)];
    part.nr_tracks = 1;
    
    /* Complete NE_NW */
    part = parts[NE_NW];
    part.next_tile = AIMap.GetTileIndex(0,-1);
    part.length = sqrt(2)/2;
    part.direction = Direction.WEST;
    part.sections = [Section(AIMap.GetTileIndex(0,0), AIRail.RAILTRACK_NW_NE)];
    part.next_part_ids = [SE_SW, SE_NW];
    part.points = array(3);
    part.points[0] = AIMap.GetTileIndex(0,0);
    part.points[1] = AIMap.GetTileIndex(1,0);
    part.points[2] = AIMap.GetTileIndex(0,1);
    part.track_start_tiles = [AIMap.GetTileIndex(0,0)];
    part.nr_tracks = 1;
    
    /* Complete NW_SW */
    part = parts[NW_SW];
    part.next_tile = AIMap.GetTileIndex(1,0);
    part.length = sqrt(2)/2;
    part.direction = Direction.SOUTH;
    part.sections = [Section(AIMap.GetTileIndex(0,0), AIRail.RAILTRACK_NW_SW)];
    part.next_part_ids = [NE_SW, NE_SE];
    part.points = array(3);
    part.points[0] = AIMap.GetTileIndex(0,0);
    part.points[1] = AIMap.GetTileIndex(1,0);
    part.points[2] = AIMap.GetTileIndex(1,1);
    part.track_start_tiles = [AIMap.GetTileIndex(0,0)];
    part.nr_tracks = 1;
    
    /* Complete SW_NW */
    part = parts[SW_NW];
    part.next_tile = AIMap.GetTileIndex(0,-1);
    part.length = sqrt(2)/2;
    part.direction = Direction.NORTH;
    part.sections = [Section(AIMap.GetTileIndex(0,0), AIRail.RAILTRACK_NW_SW)];
    part.next_part_ids = [SE_NE, SE_NW];
    part.points = array(3);
    part.points[0] = AIMap.GetTileIndex(0,0);
    part.points[1] = AIMap.GetTileIndex(1,0);
    part.points[2] = AIMap.GetTileIndex(1,1);
    part.track_start_tiles = [AIMap.GetTileIndex(0,0)];
    part.nr_tracks = 1;
    
    /* Complete NE_SE */
    part = parts[NE_SE];
    part.next_tile = AIMap.GetTileIndex(0,1);
    part.length = sqrt(2)/2;
    part.direction = Direction.SOUTH;
    part.sections = [Section(AIMap.GetTileIndex(0,0), AIRail.RAILTRACK_NE_SE)];
    part.next_part_ids = [NW_SW, NW_SE];
    part.points = array(3);
    part.points[0] = AIMap.GetTileIndex(0,0);
    part.points[1] = AIMap.GetTileIndex(0,1);
    part.points[2] = AIMap.GetTileIndex(1,1);
    part.track_start_tiles = [AIMap.GetTileIndex(0,0)];
    part.nr_tracks = 1;
    
    /* Complete SE_NE */
    part = parts[SE_NE];
    part.next_tile = AIMap.GetTileIndex(-1,0);
    part.length = sqrt(2)/2;
    part.direction = Direction.NORTH;
    part.sections = [Section(AIMap.GetTileIndex(0,0), AIRail.RAILTRACK_NE_SE)];
    part.next_part_ids = [SW_NE, SW_NW];
    part.points = array(3);
    part.points[0] = AIMap.GetTileIndex(0,0);
    part.points[1] = AIMap.GetTileIndex(0,1);
    part.points[2] = AIMap.GetTileIndex(1,1);
    part.track_start_tiles = [AIMap.GetTileIndex(0,0)];
    part.nr_tracks = 1;
    
    /**** Double parts ****/
    
    /*** Conventional parts ***/
    
    /** Line parts **/
    
    /* Complete D_NW_SE */
    part = parts[D_NW_SE];
    part.next_tile = AIMap.GetTileIndex(0,1);
    part.length = 1;
    part.direction = Direction.SOUTHEAST;
    part.sections = array(2);
    part.sections[0] = Section(AIMap.GetTileIndex(0,0), AIRail.RAILTRACK_NW_SE);
    part.sections[1] = Section(AIMap.GetTileIndex(-1,0), AIRail.RAILTRACK_NW_SE);
    part.next_part_ids = [D_NW_SE, D_N_S_LF, D_W_E_RF, D_NW_SW, D_NW_NE];
    part.points = array(6);
    part.points[0] = AIMap.GetTileIndex(0,0);
    part.points[1] = AIMap.GetTileIndex(1,0);
    part.points[2] = AIMap.GetTileIndex(-1,0);
    part.points[3] = AIMap.GetTileIndex(0,1);
    part.points[4] = AIMap.GetTileIndex(1,1);
    part.points[5] = AIMap.GetTileIndex(-1,1);
    part.track_start_tiles = [AIMap.GetTileIndex(0,0), AIMap.GetTileIndex(-1,0)];
    part.nr_tracks = 2;
    
    /* Complete D_SE_NW */
    part = parts[D_SE_NW];
    part.next_tile = AIMap.GetTileIndex(0,-1);
    part.length = 1;
    part.direction = Direction.NORTHWEST;
    part.sections = array(2);
    part.sections[0] = Section(AIMap.GetTileIndex(0,0), AIRail.RAILTRACK_NW_SE);
    part.sections[1] = Section(AIMap.GetTileIndex(1,0), AIRail.RAILTRACK_NW_SE);
    part.next_part_ids = [D_SE_NW, D_S_N_LF, D_E_W_RF, D_SE_SW, D_SE_NE];
    part.points = array(6);
    part.points[0] = AIMap.GetTileIndex(0,0);
    part.points[1] = AIMap.GetTileIndex(1,0);
    part.points[2] = AIMap.GetTileIndex(2,0);
    part.points[3] = AIMap.GetTileIndex(0,1);
    part.points[4] = AIMap.GetTileIndex(1,1);
    part.points[5] = AIMap.GetTileIndex(2,1);
    part.track_start_tiles = [AIMap.GetTileIndex(0,0), AIMap.GetTileIndex(1,0)];
    part.nr_tracks = 2;
    
    /* Complete D_SW_NE */
    part = parts[D_SW_NE];
    part.next_tile = AIMap.GetTileIndex(-1,0);
    part.length = 1;
    part.direction = Direction.NORTHEAST;
    part.sections = array(2);
    part.sections[0] = Section(AIMap.GetTileIndex(0,0), AIRail.RAILTRACK_NE_SW);
    part.sections[1] = Section(AIMap.GetTileIndex(0,-1), AIRail.RAILTRACK_NE_SW);
    part.next_part_ids = [D_SW_NE, D_S_N_RF, D_W_E_LF, D_SW_SE, D_SW_NW];
    part.points = array(6);
    part.points[0] = AIMap.GetTileIndex(0,0);
    part.points[1] = AIMap.GetTileIndex(0,-1);
    part.points[2] = AIMap.GetTileIndex(0,1);
    part.points[3] = AIMap.GetTileIndex(1,0);
    part.points[4] = AIMap.GetTileIndex(1,-1);
    part.points[5] = AIMap.GetTileIndex(1,1);
    part.track_start_tiles = [AIMap.GetTileIndex(0,0), AIMap.GetTileIndex(0,-1)];
    part.nr_tracks = 2;
    
    /* Complete D_NE_SW */
    part = parts[D_NE_SW];
    part.next_tile = AIMap.GetTileIndex(1,0);
    part.length = 1;
    part.direction = Direction.SOUTHWEST;
    part.sections = array(2);
    part.sections[0] = Section(AIMap.GetTileIndex(0,0), AIRail.RAILTRACK_NE_SW);
    part.sections[1] = Section(AIMap.GetTileIndex(0,1), AIRail.RAILTRACK_NE_SW);
    part.next_part_ids = [D_NE_SW, D_N_S_RF, D_E_W_LF, D_NE_NW, D_NE_SE];
    part.points = array(6);
    part.points[0] = AIMap.GetTileIndex(0,0);
    part.points[1] = AIMap.GetTileIndex(0,1);
    part.points[2] = AIMap.GetTileIndex(0,2);
    part.points[3] = AIMap.GetTileIndex(1,0);
    part.points[4] = AIMap.GetTileIndex(1,1);
    part.points[5] = AIMap.GetTileIndex(1,2);
    part.track_start_tiles = [AIMap.GetTileIndex(0,0), AIMap.GetTileIndex(0,1)];
    part.nr_tracks = 2;
    
    /** Diagonals **/
    
    /* Complete D_N_S_RF */
    part = parts[D_N_S_RF];
    part.next_tile = AIMap.GetTileIndex(1,1);
    part.length = sqrt(2)/2;
    part.direction = Direction.SOUTH;
    part.sections = array(4);
    part.sections[0] = Section(AIMap.GetTileIndex(0,0), AIRail.RAILTRACK_NE_SE);
    part.sections[1] = Section(AIMap.GetTileIndex(0,1), AIRail.RAILTRACK_NW_SW);
    part.sections[2] = Section(AIMap.GetTileIndex(0,1), AIRail.RAILTRACK_NE_SE);
    part.sections[3] = Section(AIMap.GetTileIndex(0,2), AIRail.RAILTRACK_NW_SW);
    part.next_part_ids = [D_N_S_RF, D_NE_SW, D_NE_SE];
    part.points = array(6);
    part.points[0] = AIMap.GetTileIndex(0,0);
    part.points[1] = AIMap.GetTileIndex(0,1);
    part.points[2] = AIMap.GetTileIndex(0,2);
    part.points[3] = AIMap.GetTileIndex(1,1);
    part.points[4] = AIMap.GetTileIndex(1,2);
    part.points[5] = AIMap.GetTileIndex(1,3);
    part.track_start_tiles = [AIMap.GetTileIndex(0,0), AIMap.GetTileIndex(0,1)];
    part.nr_tracks = 2;
    
    /* Complete D_S_N_RF */
    part = parts[D_S_N_RF];
    part.next_tile = AIMap.GetTileIndex(-1,-1);
    part.length = sqrt(2)/2;
    part.direction = Direction.NORTH;
    part.sections = array(4);
    part.sections[0] = Section(AIMap.GetTileIndex(0,0), AIRail.RAILTRACK_NW_SW);
    part.sections[1] = Section(AIMap.GetTileIndex(0,-1), AIRail.RAILTRACK_NE_SE);
    part.sections[2] = Section(AIMap.GetTileIndex(0,-1), AIRail.RAILTRACK_NW_SW);
    part.sections[3] = Section(AIMap.GetTileIndex(0,-2), AIRail.RAILTRACK_NE_SE);
    part.next_part_ids = [D_S_N_RF, D_SW_NE, D_SW_NW];
    part.points = array(6);
    part.points[0] = AIMap.GetTileIndex(0,0);
    part.points[1] = AIMap.GetTileIndex(0,-1);
    part.points[2] = AIMap.GetTileIndex(0,-2);
    part.points[3] = AIMap.GetTileIndex(1,1);
    part.points[4] = AIMap.GetTileIndex(1,0);
    part.points[5] = AIMap.GetTileIndex(1,-1);
    part.track_start_tiles = [AIMap.GetTileIndex(0,0), AIMap.GetTileIndex(0,-1)];
    part.nr_tracks = 2;
    
    /* Complete D_N_S_LF */
    part = parts[D_N_S_LF];
    part.next_tile = AIMap.GetTileIndex(1,1);
    part.length = sqrt(2)/2;
    part.direction = Direction.SOUTH;
    part.sections = array(4);
    part.sections[0] = Section(AIMap.GetTileIndex(0,0), AIRail.RAILTRACK_NW_SW);
    part.sections[1] = Section(AIMap.GetTileIndex(1,0), AIRail.RAILTRACK_NE_SE);
    part.sections[2] = Section(AIMap.GetTileIndex(-1,0), AIRail.RAILTRACK_NW_SW);
    part.sections[3] = Section(AIMap.GetTileIndex(0,0), AIRail.RAILTRACK_NE_SE);
    part.next_part_ids = [D_N_S_LF, D_NW_SE, D_NW_SW];
    part.points = array(6);
    part.points[0] = AIMap.GetTileIndex(0,0);
    part.points[1] = AIMap.GetTileIndex(-1,0);
    part.points[2] = AIMap.GetTileIndex(1,0);
    part.points[3] = AIMap.GetTileIndex(0,1);
    part.points[4] = AIMap.GetTileIndex(1,1);
    part.points[5] = AIMap.GetTileIndex(2,1);
    part.track_start_tiles = [AIMap.GetTileIndex(0,0), AIMap.GetTileIndex(-1,0)];
    part.nr_tracks = 2;
    
    /* Complete D_S_N_LF */
    part = parts[D_S_N_LF];
    part.next_tile = AIMap.GetTileIndex(-1,-1);
    part.length = sqrt(2)/2;
    part.direction = Direction.NORTH;
    part.sections = array(4);
    part.sections[0] = Section(AIMap.GetTileIndex(0,0), AIRail.RAILTRACK_NE_SE);
    part.sections[1] = Section(AIMap.GetTileIndex(-1,0), AIRail.RAILTRACK_NW_SW);
    part.sections[2] = Section(AIMap.GetTileIndex(1,0), AIRail.RAILTRACK_NE_SE);
    part.sections[3] = Section(AIMap.GetTileIndex(0,0), AIRail.RAILTRACK_NW_SW);
    part.next_part_ids = [D_S_N_LF, D_SE_NW, D_SE_NE];
    part.points = array(6);
    part.points[0] = AIMap.GetTileIndex(0,0);
    part.points[1] = AIMap.GetTileIndex(-1,0);
    part.points[2] = AIMap.GetTileIndex(1,0);
    part.points[3] = AIMap.GetTileIndex(0,1);
    part.points[4] = AIMap.GetTileIndex(1,1);
    part.points[5] = AIMap.GetTileIndex(2,1);
    part.track_start_tiles = [AIMap.GetTileIndex(0,0), AIMap.GetTileIndex(1,0)];
    part.nr_tracks = 2;
    
    /* Complete D_W_E_RF */
    part = parts[D_W_E_RF];
    part.next_tile = AIMap.GetTileIndex(-1,1);
    part.length = sqrt(2)/2;
    part.direction = Direction.EAST;
    part.sections = array(4);
    part.sections[0] = Section(AIMap.GetTileIndex(0,0), AIRail.RAILTRACK_NW_NE);
    part.sections[1] = Section(AIMap.GetTileIndex(-1,0), AIRail.RAILTRACK_SW_SE);
    part.sections[2] = Section(AIMap.GetTileIndex(-1,0), AIRail.RAILTRACK_NW_NE);
    part.sections[3] = Section(AIMap.GetTileIndex(-2,0), AIRail.RAILTRACK_SW_SE);
    part.next_part_ids = [D_W_E_RF, D_NW_SE, D_NW_NE];
    part.points = array(6);
    part.points[0] = AIMap.GetTileIndex(0,0);
    part.points[1] = AIMap.GetTileIndex(1,0);
    part.points[2] = AIMap.GetTileIndex(-1,0);
    part.points[3] = AIMap.GetTileIndex(0,1);
    part.points[4] = AIMap.GetTileIndex(-1,1);
    part.points[5] = AIMap.GetTileIndex(-2,1);
    part.track_start_tiles = [AIMap.GetTileIndex(0,0), AIMap.GetTileIndex(-1,0)];
    part.nr_tracks = 2;
    
    /* Complete D_E_W_RF */
    part = parts[D_E_W_RF];
    part.next_tile = AIMap.GetTileIndex(1,-1);
    part.length = sqrt(2)/2;
    part.direction = Direction.WEST;
    part.sections = array(4);
    part.sections[0] = Section(AIMap.GetTileIndex(0,0), AIRail.RAILTRACK_SW_SE);
    part.sections[1] = Section(AIMap.GetTileIndex(1,0), AIRail.RAILTRACK_NW_NE);
    part.sections[2] = Section(AIMap.GetTileIndex(1,0), AIRail.RAILTRACK_SW_SE);
    part.sections[3] = Section(AIMap.GetTileIndex(2,0), AIRail.RAILTRACK_NW_NE);
    part.next_part_ids = [D_E_W_RF, D_SE_NW, D_SE_SW];
    part.points = array(6);
    part.points[0] = AIMap.GetTileIndex(1,0);
    part.points[1] = AIMap.GetTileIndex(2,0);
    part.points[2] = AIMap.GetTileIndex(3,0);
    part.points[3] = AIMap.GetTileIndex(0,1);
    part.points[4] = AIMap.GetTileIndex(1,1);
    part.points[5] = AIMap.GetTileIndex(2,1);
    part.track_start_tiles = [AIMap.GetTileIndex(0,0), AIMap.GetTileIndex(1,0)];
    part.nr_tracks = 2;
    
    /* Complete D_W_E_LF */
    part = parts[D_W_E_LF];
    part.next_tile = AIMap.GetTileIndex(-1,1);
    part.length = sqrt(2)/2;
    part.direction = Direction.EAST;
    part.sections = array(4);
    part.sections[0] = Section(AIMap.GetTileIndex(0,0), AIRail.RAILTRACK_SW_SE);
    part.sections[1] = Section(AIMap.GetTileIndex(0,1), AIRail.RAILTRACK_NW_NE);
    part.sections[2] = Section(AIMap.GetTileIndex(0,-1), AIRail.RAILTRACK_SW_SE);
    part.sections[3] = Section(AIMap.GetTileIndex(0,0), AIRail.RAILTRACK_NW_NE);
    part.next_part_ids = [D_W_E_LF, D_SW_NE, D_SW_SE];
    part.points = array(6);
    part.points[0] = AIMap.GetTileIndex(0,0);
    part.points[1] = AIMap.GetTileIndex(0,1);
    part.points[2] = AIMap.GetTileIndex(0,2);
    part.points[3] = AIMap.GetTileIndex(1,-1);
    part.points[4] = AIMap.GetTileIndex(1,0);
    part.points[5] = AIMap.GetTileIndex(1,1);
    part.track_start_tiles = [AIMap.GetTileIndex(0,0), AIMap.GetTileIndex(0,-1)];
    part.nr_tracks = 2;
    
    /* Complete D_E_W_LF */
    part = parts[D_E_W_LF];
    part.next_tile = AIMap.GetTileIndex(1,-1);
    part.length = sqrt(2)/2;
    part.direction = Direction.WEST;
    part.sections = array(4);
    part.sections[0] = Section(AIMap.GetTileIndex(0,0), AIRail.RAILTRACK_NW_NE);
    part.sections[1] = Section(AIMap.GetTileIndex(0,-1), AIRail.RAILTRACK_SW_SE);
    part.sections[2] = Section(AIMap.GetTileIndex(0,1), AIRail.RAILTRACK_NW_NE);
    part.sections[3] = Section(AIMap.GetTileIndex(0,0), AIRail.RAILTRACK_SW_SE);
    part.next_part_ids = [D_E_W_LF, D_NE_SW, D_NE_NW];
    part.points = array(6);
    part.points[0] = AIMap.GetTileIndex(0,0);
    part.points[1] = AIMap.GetTileIndex(0,1);
    part.points[2] = AIMap.GetTileIndex(0,2);
    part.points[3] = AIMap.GetTileIndex(1,-1);
    part.points[4] = AIMap.GetTileIndex(1,0);
    part.points[5] = AIMap.GetTileIndex(1,1);
    part.track_start_tiles = [AIMap.GetTileIndex(0,0), AIMap.GetTileIndex(0,1)];
    part.nr_tracks = 2;
    
    /** Corners **/
    
    /* Complete D_NW_NE */
    part = parts[D_NW_NE];
    part.next_tile = AIMap.GetTileIndex(-2,1);
    part.length = sqrt(2)/2;
    part.direction = Direction.EAST;
    part.sections = array(4);
    part.sections[0] = Section(AIMap.GetTileIndex(0,0), AIRail.RAILTRACK_NW_NE);
    part.sections[1] = Section(AIMap.GetTileIndex(-1,0), AIRail.RAILTRACK_SW_SE);
    part.sections[2] = Section(AIMap.GetTileIndex(-1,1), AIRail.RAILTRACK_NW_NE);
    part.sections[3] = Section(AIMap.GetTileIndex(-1,0), AIRail.RAILTRACK_NW_NE);
    part.next_part_ids = [D_SW_NE, D_W_E_LF];
    part.points = array(6);
    part.points[0] = AIMap.GetTileIndex(0,0);
    part.points[1] = AIMap.GetTileIndex(1,0);
    part.points[2] = AIMap.GetTileIndex(0,1);
    part.points[3] = AIMap.GetTileIndex(-1,2);
    part.points[4] = AIMap.GetTileIndex(-1,1);
    part.points[5] = AIMap.GetTileIndex(-1,0);
    part.track_start_tiles = [AIMap.GetTileIndex(0,0), AIMap.GetTileIndex(-1,0)];
    part.nr_tracks = 2;
    
    /* Complete D_NE_NW */
    part = parts[D_NE_NW];
    part.next_tile = AIMap.GetTileIndex(0,-1);
    part.length = sqrt(2)/2;
    part.direction = Direction.WEST;
    part.sections = array(4);
    part.sections[0] = Section(AIMap.GetTileIndex(0,0), AIRail.RAILTRACK_NW_NE);
    part.sections[1] = Section(AIMap.GetTileIndex(0,1), AIRail.RAILTRACK_NW_NE);
    part.sections[2] = Section(AIMap.GetTileIndex(0,0), AIRail.RAILTRACK_SW_SE);
    part.sections[3] = Section(AIMap.GetTileIndex(1,0), AIRail.RAILTRACK_NW_NE);
    part.next_part_ids = [D_SE_NW, D_E_W_RF];
    part.points = array(6);
    part.points[0] = AIMap.GetTileIndex(0,0);
    part.points[1] = AIMap.GetTileIndex(0,1);
    part.points[2] = AIMap.GetTileIndex(0,2);
    part.points[3] = AIMap.GetTileIndex(1,0);
    part.points[4] = AIMap.GetTileIndex(1,1);
    part.points[5] = AIMap.GetTileIndex(2,0);
    part.track_start_tiles = [AIMap.GetTileIndex(0,0), AIMap.GetTileIndex(0,1)];
    part.nr_tracks = 2;
    
    /* Complete D_SW_SE */
    part = parts[D_SW_SE];
    part.next_tile = AIMap.GetTileIndex(0,1);
    part.length = sqrt(2)/2;
    part.direction = Direction.EAST;
    part.sections = array(4);
    part.sections[0] = Section(AIMap.GetTileIndex(0,0), AIRail.RAILTRACK_SW_SE);
    part.sections[1] = Section(AIMap.GetTileIndex(0,-1), AIRail.RAILTRACK_SW_SE);
    part.sections[2] = Section(AIMap.GetTileIndex(0,0), AIRail.RAILTRACK_NW_NE);
    part.sections[3] = Section(AIMap.GetTileIndex(-1,0), AIRail.RAILTRACK_SW_SE);
    part.next_part_ids = [D_NW_SE, D_W_E_RF];
    part.points = array(6);
    part.points[0] = AIMap.GetTileIndex(0,0);
    part.points[1] = AIMap.GetTileIndex(1,-1);
    part.points[2] = AIMap.GetTileIndex(1,0);
    part.points[3] = AIMap.GetTileIndex(1,1);
    part.points[4] = AIMap.GetTileIndex(0,1);
    part.points[5] = AIMap.GetTileIndex(-1,1);
    part.track_start_tiles = [AIMap.GetTileIndex(0,0), AIMap.GetTileIndex(0,-1)];
    part.nr_tracks = 2;
    
    /* Complete D_SE_SW */
    part = parts[D_SE_SW];
    part.next_tile = AIMap.GetTileIndex(2,-1);
    part.length = sqrt(2)/2;
    part.direction = Direction.WEST;
    part.sections = array(4);
    part.sections[0] = Section(AIMap.GetTileIndex(0,0), AIRail.RAILTRACK_SW_SE);
    part.sections[1] = Section(AIMap.GetTileIndex(1,0), AIRail.RAILTRACK_NW_NE);
    part.sections[2] = Section(AIMap.GetTileIndex(1,-1), AIRail.RAILTRACK_SW_SE);
    part.sections[3] = Section(AIMap.GetTileIndex(1,0), AIRail.RAILTRACK_SW_SE);
    part.next_part_ids = [D_NE_SW, D_E_W_LF];
    part.points = array(6);
    part.points[0] = AIMap.GetTileIndex(0,1);
    part.points[1] = AIMap.GetTileIndex(1,1);
    part.points[2] = AIMap.GetTileIndex(2,1);
    part.points[3] = AIMap.GetTileIndex(1,0);
    part.points[4] = AIMap.GetTileIndex(2,0);
    part.points[5] = AIMap.GetTileIndex(2,1);
    part.track_start_tiles = [AIMap.GetTileIndex(0,0), AIMap.GetTileIndex(1,0)];
    part.nr_tracks = 2;
    
    /* Complete D_NE_SE */
    part = parts[D_NE_SE];
    part.next_tile = AIMap.GetTileIndex(1,2);
    part.length = sqrt(2)/2;
    part.direction = Direction.SOUTH;
    part.sections = array(4);
    part.sections[0] = Section(AIMap.GetTileIndex(0,0), AIRail.RAILTRACK_NE_SE);
    part.sections[1] = Section(AIMap.GetTileIndex(0,1), AIRail.RAILTRACK_NW_SW);
    part.sections[2] = Section(AIMap.GetTileIndex(1,1), AIRail.RAILTRACK_NE_SE);
    part.sections[3] = Section(AIMap.GetTileIndex(0,1), AIRail.RAILTRACK_NE_SE);
    part.next_part_ids = [D_NW_SE, D_N_S_LF];
    part.points = array(6);
    part.points[0] = AIMap.GetTileIndex(0,0);
    part.points[1] = AIMap.GetTileIndex(0,1);
    part.points[2] = AIMap.GetTileIndex(0,2);
    part.points[3] = AIMap.GetTileIndex(1,1);
    part.points[4] = AIMap.GetTileIndex(1,2);
    part.points[5] = AIMap.GetTileIndex(2,2);
    part.track_start_tiles = [AIMap.GetTileIndex(0,0), AIMap.GetTileIndex(0,1)];
    part.nr_tracks = 2;
    
    /* Complete D_SE_NE */
    part = parts[D_SE_NE];
    part.next_tile = AIMap.GetTileIndex(-1,0);
    part.length = sqrt(2)/2;
    part.direction = Direction.NORTH;
    part.sections = array(4);
    part.sections[0] = Section(AIMap.GetTileIndex(0,0), AIRail.RAILTRACK_NE_SE);
    part.sections[1] = Section(AIMap.GetTileIndex(1,0), AIRail.RAILTRACK_NE_SE);
    part.sections[2] = Section(AIMap.GetTileIndex(0,0), AIRail.RAILTRACK_NW_SW);
    part.sections[3] = Section(AIMap.GetTileIndex(0,-1), AIRail.RAILTRACK_NE_SE);
    part.next_part_ids = [D_SW_NE, D_S_N_RF];
    part.points = array(6);
    part.points[0] = AIMap.GetTileIndex(0,0);
    part.points[1] = AIMap.GetTileIndex(0,-1);
    part.points[2] = AIMap.GetTileIndex(0,1);
    part.points[3] = AIMap.GetTileIndex(1,0);
    part.points[4] = AIMap.GetTileIndex(1,1);
    part.points[5] = AIMap.GetTileIndex(2,1);
    part.track_start_tiles = [AIMap.GetTileIndex(0,0), AIMap.GetTileIndex(1,0)];
    part.nr_tracks = 2;
    
    /* Complete D_NW_SW */
    part = parts[D_NW_SW];
    part.next_tile = AIMap.GetTileIndex(1,0);
    part.length = sqrt(2)/2;
    part.direction = Direction.SOUTH;
    part.sections = array(4);
    part.sections[0] = Section(AIMap.GetTileIndex(0,0), AIRail.RAILTRACK_NW_SW);
    part.sections[1] = Section(AIMap.GetTileIndex(-1,0), AIRail.RAILTRACK_NW_SW);
    part.sections[2] = Section(AIMap.GetTileIndex(0,0), AIRail.RAILTRACK_NE_SE);
    part.sections[3] = Section(AIMap.GetTileIndex(0,1), AIRail.RAILTRACK_NW_SW);
    part.next_part_ids = [D_NE_SW, D_N_S_RF];
    part.points = array(6);
    part.points[0] = AIMap.GetTileIndex(0,0);
    part.points[1] = AIMap.GetTileIndex(1,0);
    part.points[2] = AIMap.GetTileIndex(1,1);
    part.points[3] = AIMap.GetTileIndex(1,2);
    part.points[4] = AIMap.GetTileIndex(0,1);
    part.points[5] = AIMap.GetTileIndex(-1,0);
    part.track_start_tiles = [AIMap.GetTileIndex(0,0), AIMap.GetTileIndex(0,-1)];
    part.nr_tracks = 2;
    
    /* Complete D_SW_NW */
    part = parts[D_SW_NW];
    part.next_tile = AIMap.GetTileIndex(-1,-2);
    part.length = sqrt(2)/2;
    part.direction = Direction.NORTH;
    part.sections = array(4);
    part.sections[0] = Section(AIMap.GetTileIndex(0,0), AIRail.RAILTRACK_NW_SW);
    part.sections[1] = Section(AIMap.GetTileIndex(0,-1), AIRail.RAILTRACK_NE_SE);
    part.sections[2] = Section(AIMap.GetTileIndex(-1,-1), AIRail.RAILTRACK_NW_SW);
    part.sections[3] = Section(AIMap.GetTileIndex(0,-1), AIRail.RAILTRACK_NW_SW);
    part.next_part_ids = [D_SE_NW, D_S_N_LF];
    part.points = array(6);
    part.points[0] = AIMap.GetTileIndex(0,0);
    part.points[1] = AIMap.GetTileIndex(1,1);
    part.points[2] = AIMap.GetTileIndex(1,0);
    part.points[3] = AIMap.GetTileIndex(1,-1);
    part.points[4] = AIMap.GetTileIndex(0,-1);
    part.points[5] = AIMap.GetTileIndex(-1,-1);
    part.track_start_tiles = [AIMap.GetTileIndex(0,0), AIMap.GetTileIndex(0,-1)];
    part.nr_tracks = 2;
}


function TrackParts::InitializePartNames()
{
    part_names = array(TrackParts.HIGHEST_PART_NUMBER + 1)
    part_names[0] = "NW_SE";
    part_names[1] = "SE_NW";
    part_names[2] = "SW_NE";
    part_names[3] = "NE_SW";
    part_names[4] = "SW_SE";
    part_names[5] = "SE_SW";
    part_names[6] = "NW_NE";
    part_names[7] = "NE_NW";
    part_names[8] = "NW_SW";
    part_names[9] = "SW_NW";
    part_names[10] = "NE_SE";
    part_names[11] = "SE_NE";
    part_names[100] = "D_NW_SE";
    part_names[101] = "D_SE_NW";
    part_names[102] = "D_SW_NE";
    part_names[103] = "D_NE_SW";
    part_names[104] = "D_N_S_RF";
    part_names[105] = "D_S_N_RF";
    part_names[106] = "D_N_S_LF";
    part_names[107] = "D_S_N_LF";
    part_names[108] = "D_W_E_RF";
    part_names[109] = "D_E_W_RF";
    part_names[110] = "D_W_E_LF";
    part_names[111] = "D_E_W_LF";
    part_names[112] = "D_NW_NE";
    part_names[113] = "D_NE_NW";
    part_names[114] = "D_SW_SE";
    part_names[115] = "D_SE_SW";
    part_names[116] = "D_NE_SE";
    part_names[117] = "D_SE_NE";
    part_names[118] = "D_NW_SW";
    part_names[119] = "D_SW_NW";
    part_names[42] = "BRIDGE_SEGMENT";
    part_names[43] = "TUNNEL_SEGMENT";
    return part_names;
}


function TrackParts::IsConventionalPart(part_id)
{
    return part_id >= 0 && part_id <= 11 || part_id >= 100 && part_id <= 119;
}


function TrackParts::IsStraight(part_id)
{
    return TrackParts.IsLine(part_id) || TrackParts.IsDiagonal(part_id);
}


function TrackParts::IsDiagonal(part_id)
{
    return part_id >= 4 && part_id <= 11 || part_id >= 104 && part_id <= 111;
}


function TrackParts::IsLine(part_id)
{
    return part_id >= 0 && part_id <= 3 || part_id >= 100 && part_id <= 103;
}


function TrackParts::IsSingleTrack(part_id)
{
    return part_id >= 0 && part_id <= 11;
}


function TrackParts::IsDoubleTrack(part_id)
{
    return part_id >= 100 && part_id <= 119;
}


/**
*   This function would like a lot of empty space at the top of the map. It's going to show you all parts
*/
function TrackParts::BuildAllParts()
{
    local rb = RailBuilder();
    local tile = AIMap.GetTileIndex(10,10);
    local number = 0;
    for(local i = 0; i <= TrackParts.HIGHEST_PART_NUMBER; i++)
    {
        if(parts[i] == null) continue;
        if(!rb.BuildPartLoc(PartLocation(i, tile))) return false;
        local exec = AIExecMode();
        AISign.BuildSign(tile, part_names[i]);
        number++;
        if(number == 6)
        {
            number = 0;
            tile += AIMap.GetTileIndex(5,-25);
        }
        else tile += AIMap.GetTileIndex(0,5);
    }
    
    return true;
}


/**
*   This function would like a lot of empty space at the top of the map. It's going to show you all parts and connect
*   all of them to their possible next parts
*/
function TrackParts::BuildAllPartsWithNexts()
{
    local rb = RailBuilder();
    local tile = AIMap.GetTileIndex(10,10);
    local number = 0;
    for(local i = 0; i <= TrackParts.HIGHEST_PART_NUMBER; i++)
    {
        if(parts[i] == null) continue;
        foreach(part_id in parts[i].next_part_ids)
        {
            if(!rb.BuildPartLoc(PartLocation(i, tile))) return false;
            local exec = AIExecMode();
            AISign.BuildSign(tile, part_names[i]);
            if(!rb.BuildPartLoc(PartLocation(part_id, tile + parts[i].next_tile))) return false;
            local exec = AIExecMode();
            AISign.BuildSign(tile + parts[i].next_tile, part_names[part_id]);
            number++;
            if(number == 6)
            {
                number = 0;
                tile += AIMap.GetTileIndex(5,-25);
            }
            else tile += AIMap.GetTileIndex(0,5);
        }
    }
    
    return true;
}