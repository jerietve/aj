class RailStationBuilder
{
    /**
    * Builds a new station. But wait! There's less.
    */
    function BuildNewNearIndustry(industry_id, platforms, length, producing, accepting)
    {
        local possible_tile_ids = RailStationBuilder.FindStationTileNearIndustry(industry_id, platforms, length, producing, accepting);
        
        //random location
        local picked_random = Util.GetRandomEntry(possible_tile_ids);
        //just one of the directions
        local track_direction;
        if(possible_tile_ids.GetValue(picked_random) & AIRail.RAILTRACK_NW_SE) track_direction = AIRail.RAILTRACK_NW_SE;
        else track_direction = AIRail.RAILTRACK_NE_SW
        
        AILog.Info("Building at " + Tile.ToString(picked_random) + ", " + track_direction);
        if(!AIRail.BuildRailStation(AIIndustry.GetLocation(industry_id), track_direction, platforms, length, AIStation.STATION_NEW))
        {
            AILog.Info(AIError.GetLastErrorString());
            return null;
        }
        
        return SimpleRailStation(picked_random, platforms);
    }
    
    
    function FindStationTileNearIndustry(industry_id, platforms, length, producing, accepting)
    {
        local radius = AIStation.GetCoverageRadius(AIStation.STATION_TRAIN);
        
        /* Get the list of tiles which are either for producing or accepting, or both */
        local tile_ids;
        local producing_tile_ids;
        local accepting_tile_ids;
        if(producing) producing_tile_ids = AITileList_IndustryProducing(industry_id, radius);
        if(accepting) accepting_tile_ids = AITileList_IndustryAccepting(industry_id, radius);
        if(producing && accepting)
        {
            producing_tile_ids.KeepList(accepting_tile_ids);
            tile_ids = producing_tile_ids;
        }
        else if(producing) tile_ids = producing_tile_ids;
        else tile_ids = accepting_tile_ids;
        
        local buildable_tile_ids = StationLocationFinder.GetCanBuildList(tile_ids, AIIndustry.GetLocation(industry_id), platforms, length);
        return StationLocationFinder.GetCanExitList(buildable_tile_ids, platforms, length);
    }
}