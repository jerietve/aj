class Tester
{
    /**
    * Let the spiraler walk out from center and place signs at all tiles within radius from center.
    *
    * @param    center      The tile to spiral out from
    * @param    direction   The direction to start in
    * @param    clockwise   Spiral clockwise or counterclockwise?
    * @param    radius      The maximum distance to move away from center
    *
    * @return   nothing
    */
    static function TestSpiraler(center, direction, clockwise, radius)
    {
        AILog.Info("Testing Spiraler");
        AILog.Info("center = " + Tile.ToString(center));
        AILog.Info("direction = " + Direction.ToString(direction));
        AILog.Info("clockwise = " + clockwise);
        AILog.Info("radius = " + radius);
        
        AIExecMode();
        local spiraler = Spiraler(center, direction, clockwise);
        local next = spiraler.Next();
        
        while(AIMap.DistanceMax(center, next) <= radius)
        {
            AISign.BuildSign(next, "x");
            next = spiraler.Next();
        }
    }
}