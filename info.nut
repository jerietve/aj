class AJ extends AIInfo
{
    function GetAuthor()      { return "Jeroen Rietveld"; }
    function GetName()        { return "Artificial Jarod"; }
    function GetDescription() { return "An AI designed to play as realistically (human-like) as possible"; }
    function GetVersion()     { return 1; }
    function GetDate()        { return "2013-04-19"; }
    function CreateInstance() { return "AJ"; }
    function GetShortName()   { return "ARTJ"; }
    function GetAPIVersion()  { return "1.5"; }
}

// Tell the core we are an AI
RegisterAI(AJ());