class SetupManager
{
    /**
    * Main function that calls all the other initializing functions, and does some simple initialization internally.
    *
    * @return   nothing
    */
    static function Initialize()
    {
        /* Don't pay interest until something interesting is about to be done */
        AICompany.SetLoanAmount(0);
        
        /* Initialize the settings */
        ai_instance.settings = Settings();
        ai_instance.settings.Init();
                    
        /* Give the AI a nice name */
        SetupManager.NameAI();
        
        /* Set at least some rail type (never set it back to invalid) */
        AIRail.SetCurrentRailType(AIRailTypeList().Begin());
    }
    
    
    /**
    * Give the AI a nice name, and set the president name and gender.
    *
    * @return   Nothing
    */
    static function NameAI()
    {
        /* Give the AI a nice name. If the default name is already taken by another instance, add a number */
        if(!AICompany.SetName("Transai"))
        {
            local i = 2;
            while(!AICompany.SetName("Transai #" + i))
            {
                i++;
            }
        }
        
        /* Set the president too */
        AICompany.SetPresidentName("Big Willie");
        AICompany.SetPresidentGender(AICompany.GENDER_MALE);
    }
}