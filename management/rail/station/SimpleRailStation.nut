class SimpleRailStation
{
    /* int: The ID used by OpenTTD */
    _station_id = -1;
    /* int: The number of platforms */
    _platforms = -1;
    /* int: The length of all these platforms */
    _length = -1;
    /* RailTrack: The direction of the track */
    _direction = -1;
    /* int: The tile ID of the north corner */
    _base_tile_id = -1;
    /* The track part forming the base end of the station */
    _base_exit_part_loc = -1;
    /* The track part forming the non-base end of the station */
    _other_exit_part_loc = -1;
    _base_exit_front_tile = -1;
    _other_exit_front_tile = -1;
    
    
    constructor(base_tile_id, platforms)
    {
        if(platforms != 2) throw "SimpleRailStation currently only supports stations with two platforms";
        
        _base_tile_id = base_tile_id;
        _platforms = platforms;
        
        _station_id = AIStation.GetStationID(_base_tile_id);
        _direction = AIRail.GetRailTracks(_base_tile_id);
        _length = AITileList_StationType(_station_id, AIStation.STATION_TRAIN).Count() / _platforms;
        
        if(_direction == AIRail.RAILTRACK_NE_SW)
        {
            _base_exit_part_loc = PartLocation(TrackParts.D_SW_NE, _base_tile_id + AIMap.GetTileIndex(-1, 1));
            _other_exit_part_loc = PartLocation(TrackParts.D_NE_SW, _base_tile_id + AIMap.GetTileIndex(_length, 0));
            _base_exit_front_tile = _base_tile_id + AIMap.GetTileIndex(-1, 1);
            _other_exit_front_tile = _base_tile_id + AIMap.GetTileIndex(_length, 0);
        }
        else
        {
            _base_exit_part_loc = PartLocation(TrackParts.D_SE_NW, _base_tile_id + AIMap.GetTileIndex(0, -1));
            _other_exit_part_loc = PartLocation(TrackParts.D_NW_SE, _base_tile_id + AIMap.GetTileIndex(1, _length));
            _base_exit_front_tile = _base_tile_id + AIMap.GetTileIndex(0, -1);
            _other_exit_front_tile = _base_tile_id + AIMap.GetTileIndex(1, _length);
        }
    }
}