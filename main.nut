require("include.nut");

/* A global variable in the root table that will contain a reference to this AI for storing global variables */
ai_instance <- null;

class AJ extends AIController
{
    settings = null;
    tp = null;
    
    constructor()
    {
        ai_instance = this;
        tp = TrackParts();  
    }
    
    
    /**
    * Contains the entire game loop. This function is entered in the beginning and never exited.
    *
    * @return   nothing
    */
    function Start()
    {
        /* Get everything up and running */
        SetupManager.Initialize();
        Test();
        
        while(true)
        {
        }
    }
    
    
    /**
    * Saves the AI state so it can be reloaded later. Currently not implemented
    *
    * @rerurn   nothing
    */
    function Save()
    {
        //TODO: implement
        local saveTable = {};
        return saveTable;
    }
    
    
    /**
    * Load a previously saved state of the AI and restore it. Currently not implemented 
    *
    * @param    version The version of AJ that saved this state
    * @param    data    The saved data
    *
    * @return   nothing
    */
    function Load(version, data)
    {
    }
    
    
    /**
    * Runs whatever code I quickly want to try out
    *
    * @return   nothing
    */
    function Test()
    {       
        local cargo_list = AICargoList();
        cargo_list.Valuate(AICargo.HasCargoClass, AICargo.CC_BULK);
        cargo_list.KeepValue(1);
        cargo_list.Valuate(AICargo.GetCargoIncome, 100, 20);
        local cargo_id = cargo_list.Begin();
        local industry_list = AIIndustryList_CargoProducing(cargo_id);
        industry_list.Valuate(AIIndustry.GetLastMonthProduction, cargo_id);
        local industry_id = industry_list.Begin();
        
        RailStationBuilder.BuildNewNearIndustry(industry_id, 1, 5, true, false);
    }
}