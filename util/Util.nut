class Util
{
    /**
    * If you want to research a rectangular area around a tile for whatever reason, this is the function for you. It
    * returns the minimum and maximum x- and y-values such that you don't access tiles outside the map. They are
    * returned in an array [x_min, x_max, y__min, y_max]
    *
    * @param    center      The tile to check around
    * @param    radius_x    The amount of tiles away from the center to include in positive and negative x-direction
    * @param    radius_y    Optional. The amount of tiles away from the center to include in positive and negative
    *                       y-direction. Defaults to radius_x
    *
    * @return   The four extreme x- and y-values in an array
    */
    static function GetExtremesRectAroundTile(center, radius_x, radius_y = null)
    {
        if(radius_y == null) radius_y = radius_x;       
        local center_x = AIMap.GetTileX(center);
        local center_y = AIMap.GetTileY(center);
        local result = [];
        result.push(max(ai_instance.settings.MIN_VALID_TILE, center_x - radius_x));
        result.push(min(AIMap.GetMapSizeX() - 2, center_x + radius_x));
        result.push(max(ai_instance.settings.MIN_VALID_TILE, center_y - radius_y));
        result.push(min(AIMap.GetMapSizeY() - 2, center_y + radius_y));
        return result;
    }
    
    
    /**
    * Get an array of all tiles within a rectangle around another tile.
    *
    * @param    center      The tile to check around
    * @param    radius_x    The amount of tiles away from the center to include in positive and negative x-direction
    * @param    radius_y    Optional. The amount of tiles away from the center to include in positive and negative
    *                       y-direction. Defaults to radius_x
    *
    * @return   An array of tiles
    */
    static function GetTilesInRectAroundTile(center, radius_x, radius_y = null)
    {
        local result = [];
        local extremes = Util.GetExtremesRectAroundTile(center, radius_x, radius_y);
        
        for(local x = extremes[0]; x <= extremes[1]; x++)
        {
            for(local y = extremes[2]; y <= extremes[3]; y++)
            {
                result.push(AIMap.GetTileIndex(x, y));
            }
        }
        
        return result;
    }
    
    
    /**
     * Gets a random entry from a list. Changes the location of the list mark.
     *
     * @param   AIList  list    The list to get an item from. Must not be empty.
     *
     * @return  int A random item from list
     */
    static function GetRandomEntry(list)
    {
        local random = AIBase.Rand();
        random %= list.Count();
        
        if(random == 0) return list.Begin();
        list.Begin();
        for(local i = 0; i < random - 1; i++)
        {
            list.Next();
        }
        
        return list.Next();
    }
    
    
    /**
    *   Walks through the list calling Equals(item) on all items of the list and returns true as soon as this returns
    *   true, or false once it reaches the start/end of the list.
    *
    *   @param  list        array
    *   @param  item        Object
    *   @param  forward     boolean True if you want to search the list forward, false for backward
    *
    *   @return boolean True if this item is in the list, false otherwise
    */
    static function InList(list, item, forward)
    {
        if(forward)
        {
            for(local i = 0; i < list.len(); i++)
            {
                if(list[i].Equals(item)) return true;
            }
            return false;
        }
        else
        {           
            for(local i = list.len()-1; i >= 0; i--)
            {
                if(list[i].Equals(item)) return true;
            }
            return false;
        }
    }
    
    
    /**
    *   Returns true if all elements of the array == each other, false otherwise
    */
    static function AllTheSame(array)
    {
        for(local i = 1; i < array.len(); i++)
        {
            if(array[i] != array[i-1]) return false;
        }
        return true;
    }


    /**
    *   Returns the index of the array which has the lowest value
    *
    *   @param  array   Numerical array
    */
    static function LowestValueIndex(array)
    {
        local minimum = array[0];
        local minimum_index = 0;
        for(local i = 1; i < array.len(); i++)
        {
            if(array[i] < array[i-1])
            {
                minimum = array[i];
                minimum_index = i;
            }
        }
        return minimum_index;
    }
}