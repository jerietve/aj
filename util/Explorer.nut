class Explorer
{
    /**
    * Places signs on all tiles that this industry accepts cargo from or supplies cargo to for a station with a certain
    * radius. These are not the only places a station could be placed to cover the industry. For that, only one tile
    * at least needs to coincide with the tiles flagged here.
    *
    * @param    industry_id The ID of the industry
    * @param    radius      The radius of the station
    * @param    accepting   True to check for accepting, false to check for supplying (currently not supported)
    *
    * @return   nothing
    */
    static function ShowIndustryCoverageArea(industry_id, radius, accepting)
    {
        local mode = AIExecMode();
        local tile_list;
        if(accepting)
        {
            tile_list = AITileList_IndustryAccepting(industry_id, radius);
        }
        else
        {
            tile_list = AITileList_IndustryProducing(industry_id, radius);
        }
        for(local tile = tile_list.Begin(); !tile_list.IsEnd(); tile = tile_list.Next())
        {
            AISign.BuildSign(tile, "x");
        }
    }
    
    
    /**
    * Places signs on all tiles that, if a station of the specified size would be built here, would result in a station
    * accepting or supplying cargo for this industry.
    *
    * @param    industry_id     The ID of the industry
    * @param    station_size_x  The 'width' of the station in x-direction
    * @param    station_size_y  The 'height' of the station in y-direction
    * @param    radius          The radius of the station
    * @param    accepting       True to check for accepting, false to check for supplying (currently not supported)
    *
    * @return   nothing
    */
    static function ShowIndustryCoverageAreaStation(industry_id, station_size_x, station_size_y, radius, accepting)
    {
        local mode = AIExecMode();
        local cargo_id;
        if(accepting) cargo_id = AICargoList_IndustryAccepting(industry_id).Begin();
        else cargo_id = AICargoList_IndustryProducing(industry_id).Begin();
        local tile_list = TileFinder.InRangeNearIndustry(industry_id, cargo_id, station_size_x, station_size_y, radius, accepting);
        for(local tile = tile_list.Begin(); !tile_list.IsEnd(); tile = tile_list.Next())
        {
            AISign.BuildSign(tile, "x");
        }
    }
    
    
    /**
    * Places signs at every tile within radius from center, with the rounded flatness value printed on it.
    *
    * @param    center              The tile to check around
    * @param    radius              The radius to place signs within
    * @param    flatness_radius_x   The x-radius to use for the flatness calculation
    * @param    flatness_radius_y   The x-radius to use for the flatness calculation
    *
    * @return   nothing
    */
    static function ShowFlatness(center, radius, flatness_radius_x, flatness_radius_y)
    {
        AILog.Info("Showing flatness around " + Tile.ToString(center));
        local mode = AIExecMode();
        local tiles = Util.GetTilesInRectAroundTile(center, radius);
        foreach(tile in tiles)
        {
            // Use abs to convert to integer
            AISign.BuildSign(tile, "" + abs(Heuristics.Flatness(tile, flatness_radius_x, flatness_radius_y)));
        }
    }
    
    
    /**
    * Prints a list of all cargo IDs with their label
    */
    static function PrintCargoLabels()
    {
        local cargo_id_list = AICargoList();
        AILog.Info("Cargo list:")
        local cargo_id = cargo_id_list.Begin();
        for(local i = 0; i < cargo_id_list.Count(); i++)
        {
            AILog.Info(cargo_id + "   " + AICargo.GetCargoLabel(cargo_id));
            cargo_id = cargo_id_list.Next();
        }
    }
}