class Flagger
{
    /**
     * Places signs on each tile in the list. The sign will display the value from the list.
     *
     * @return  nothing
     */
    function FlagTileList(list)
    {
        AIExecMode();
        foreach(tile_id, value in list)
        {
            AISign.BuildSign(tile_id, "" + value);
        }
    }
    
    
    function FlagIndustryProducingTiles()
    {
        local radius = AIStation.GetCoverageRadius(AIStation.STATION_TRAIN);
        local industry_list = AIIndustryList();
        
        foreach(industry_id, value in industry_list)
        {
            local tile_list_industry_accepting = AITileList_IndustryAccepting(industry_id, radius);
            Flagger.FlagTileList(tile_list_industry_accepting);
        }
    }
}