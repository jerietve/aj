class Edge
{
    static NORTHEAST = 0;
    static SOUTHEAST = 1;
    static SOUTHWEST = 2;
    static NORTHWEST = 3;
    
    
    /**
    *   Returns the tile on the other side of this edge of tile, also if this is not a valid tile, or null in case the
    *   edge is not valid.
    */
    static function GetNeighbour(tile, edge)
    {
        switch(edge)
        {
            case Edge.NORTHEAST:
                return tile + AIMap.GetTileIndex(-1,0);
            case Edge.SOUTHEAST:
                return tile + AIMap.GetTileIndex(0,1);
            case Edge.SOUTHWEST:
                return tile + AIMap.GetTileIndex(1,0);
            case Edge.NORTHWEST:
                return tile + AIMap.GetTileIndex(0,-1);
            default:
                return null;
        }
    }
    
    
    /**
    *   Returns the edge you started from f you follow track in direction
    */
    static function GetFromEdgeTrackDirection(track, direction)
    {
        switch(track)
        {
            case AIRail.RAILTRACK_NE_SW:
                if(direction == Direction.SOUTHWEST) return Edge.NORTHEAST;
                else if(direction == Direction.NORTHEAST) return Edge.SOUTHWEST;
                else throw("Invalid track-direction combination for Edge.GetToEdgeTrackDirection: track = " + track + ", direction = " + direction);
            case AIRail.RAILTRACK_NW_SE:
                if(direction == Direction.NORTHWEST) return Edge.SOUTHEAST;
                else if(direction == Direction.SOUTHEAST) return Edge.NORTHWEST;
                else throw("Invalid track-direction combination for Edge.GetToEdgeTrackDirection: track = " + track + ", direction = " + direction);
            case AIRail.RAILTRACK_NW_NE:
                if(direction == Direction.EAST) return Edge.NORTHWEST;
                else if(direction == Direction.WEST) return Edge.NORTHEAST;
                else throw("Invalid track-direction combination for Edge.GetToEdgeTrackDirection: track = " + track + ", direction = " + direction);
            case AIRail.RAILTRACK_SW_SE:
                if(direction == Direction.EAST) return Edge.SOUTHWEST;
                else if(direction == Direction.WEST) return Edge.SOUTHEAST;
                else throw("Invalid track-direction combination for Edge.GetToEdgeTrackDirection: track = " + track + ", direction = " + direction);
            case AIRail.RAILTRACK_NW_SW:
                if(direction == Direction.NORTH) return Edge.SOUTHWEST;
                else if(direction == Direction.SOUTH) return Edge.NORTHWEST;
                else throw("Invalid track-direction combination for Edge.GetToEdgeTrackDirection: track = " + track + ", direction = " + direction);
            case AIRail.RAILTRACK_NE_SE:
                if(direction == Direction.NORTH) return Edge.SOUTHEAST;
                else if(direction == Direction.SOUTH) return Edge.NORTHEAST;
                else throw("Invalid track-direction combination for Edge.GetToEdgeTrackDirection: track = " + track + ", direction = " + direction);
            default:
                throw("Invalid track for Edge.GetToEdgeTrackDirection: track = " + track)
        }
    }
    
    
    static function Opposite(edge)
    {
        return (edge + 2) % 4;
    }
}