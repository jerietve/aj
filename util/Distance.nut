class Distance
{
    /**
    * Get the distance between two tiles measured along a main direction. The distance is always positive.
    *
    * @param    tile1       One tile
    * @param    tile2       Another tile
    * @param    direction   The direction along which to measure
    *
    * @pre  Direction.IsMainDirection(direction)
    *
    * @return   The distance between the tiles along the direction
    */
    static function BetweenTilesDirection(tile1, tile2, direction)
    {
        switch(direction)
        {
            case Direction.NORTHEAST:
            case Direction.SOUTHWEST:
                return abs(AIMap.GetTileX(tile1) - AIMap.GetTileX(tile2));
            case Direction.NORTHWEST:
            case Direction.SOUTHEAST:
                return abs(AIMap.GetTileY(tile1) - AIMap.GetTileY(tile2));
        }
    }
    
    
    /**
    *   Calculates the shortest possible distance by train from source to destination
    *
    *   @param  source_x    int     Source x location
    *   @param  source_y    int     Source y location
    *   @param  dest_x      int     Destination x location
    *   @param  dest_y      int     Destination y location
    *
    *   @return int The distance by train track
    */
    static function BetweenCoordinates(source_x, source_y, dest_x, dest_y)
    {
        return abs(abs(source_x - dest_x) - abs(source_y - dest_y)) + sqrt(2) * min(abs(source_x - dest_x), abs(source_y - dest_y));
    }
    
    
    /**
    *   Calculates the shortest possible distance by train from source to destination
    *
    *   @param  source  AITile  Source tile
    *   @param  dest    AITile  Destination tile
    *
    *   @return int The distance by train track
    */
    static function BetweenTiles(source, dest)
    {
        local source_x = AIMap.GetTileX(source);
        local source_y = AIMap.GetTileY(source);
        local dest_x = AIMap.GetTileX(dest);
        local dest_y = AIMap.GetTileY(dest);
        return Util.Distance(source_x, source_y, dest_x, dest_y);
    }
}