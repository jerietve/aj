class AbstractDirection
{
    static N_S               = 0;
    static NE_SW             = 1;
    static E_W               = 2;
    static NW_SE             = 3;
    static INVALID_DIRECTION = 4;
    
    function GetDirection(from_tile, to_tile)
    {
        local f_x = AIMap.GetTileX(from_tile);
        local f_y = AIMap.GetTileY(from_tile);
        local t_x = AIMap.GetTileX(to_tile);
        local t_y = AIMap.GetTileY(to_tile);
        
        if(f_x == t_x) return AbstractDirection.NW_SE;
        else if(f_y == t_y) return AbstractDirection.NE_SW;
        else if(f_x - t_x == f_y - t_y) return AbstractDirection.N_S;
        else return AbstractDirection.E_W;
    }
    
    
    function AllowsRailroadCrossing(direction)
    {
        return direction == AbstractDirection.NE_SW || direction == AbstractDirection.NW_SE;
    }
    
    
    function AllowsBridge(direction)
    {
        return direction == AbstractDirection.NE_SW || direction == AbstractDirection.NW_SE;
    }
    
    
    /**
    *   Returns an array with the direction and the two adjacent abstract directions, in clockwise order
    */
    function AdjacentDirections(direction)
    {
        local result = [(direction - 1) % 4, direction, (direction + 1) % 4];
        return result;
    }
}