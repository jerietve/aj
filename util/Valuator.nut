class Valuator
{
    static function IndustryToTileDistance(industry_id, tile_id)
    {
        return ceil(Map.DistanceTiles(AIIndustry.GetLocation(industry_id), tile_id)).tointeger();
    }
}