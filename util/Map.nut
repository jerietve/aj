class Map
{
    /**
    *   Calculates the shortest possible distance by train from source to destination
    *
    *   @param  source_x    int     Source x location
    *   @param  source_y    int     Source y location
    *   @param  dest_x      int     Destination x location
    *   @param  dest_y      int     Destination y location
    *
    *   @return int The distance by train track
    */
    static function Distance(source_x, source_y, dest_x, dest_y)
    {
        return abs(abs(source_x - dest_x) - abs(source_y - dest_y)) + sqrt(2) * Math.Minimum(abs(source_x - dest_x), abs(source_y - dest_y));
    }
    
    
    /**
    *   Calculates the shortest possible distance by train from source to destination
    *
    *   @param  source  AITile  Source tile
    *   @param  dest    AITile  Destination tile
    *
    *   @return int The distance by train track
    */
    static function DistanceTiles(source, dest)
    {
        local source_x = AIMap.GetTileX(source);
        local source_y = AIMap.GetTileY(source);
        local dest_x = AIMap.GetTileX(dest);
        local dest_y = AIMap.GetTileY(dest);
        return Map.Distance(source_x, source_y, dest_x, dest_y);
    }
}