class Tile
{
    /**
    * Get a neighbouring tile in a certain direction.
    *
    * @param    tile        The tile to check from
    * @param    direction   The direction to the neighbour
    *
    * @return The neighbouring tile's index, which will be invalid if it is outside the map
    */
    static function GetAdjacentTile(tile, direction)
    {
        switch(direction)
        {
            case Direction.NORTH:
                return tile + AIMap.GetTileIndex(-1,-1);
            case Direction.NORTHEAST:
                return tile + AIMap.GetTileIndex(-1,0);
            case Direction.EAST:
                return tile + AIMap.GetTileIndex(-1,1);
            case Direction.SOUTHEAST:
                return tile + AIMap.GetTileIndex(0,1);
            case Direction.SOUTH:
                return tile + AIMap.GetTileIndex(1,1);
            case Direction.SOUTHWEST:
                return tile + AIMap.GetTileIndex(1,0);
            case Direction.WEST:
                return tile + AIMap.GetTileIndex(1,-1);
            case Direction.NORTHWEST:
                return tile + AIMap.GetTileIndex(0,-1);
            default:
                return -1;
        }
    }
    
    
    /**
    * Get a tile a certain distance in a certain direction from another tile.
    *
    * @param    tile        The tile to check from
    * @param    direction   The direction to the other tile
    * @param    distance    The distance to the other tile
    *
    * @return The found tile's index, which will be invalid if it is outside the map
    */
    static function GetTileInDirection(tile, direction, distance)
    {
        local result;
        switch(direction)
        {
            case Direction.NORTH:
                result = tile + AIMap.GetTileIndex(-distance,-distance);
                break;
            case Direction.NORTHEAST:
                result = tile + AIMap.GetTileIndex(-distance,0);
                break;
            case Direction.EAST:
                result = tile + AIMap.GetTileIndex(-distance,distance);
                break;
            case Direction.SOUTHEAST:
                result = tile + AIMap.GetTileIndex(0,distance);
                break;
            case Direction.SOUTH:
                result = tile + AIMap.GetTileIndex(distance,distance);
                break;
            case Direction.SOUTHWEST:
                result = tile + AIMap.GetTileIndex(distance,0);
                break;
            case Direction.WEST:
                result = tile + AIMap.GetTileIndex(distance,-distance);
                break;
            case Direction.NORTHWEST:
                result = tile + AIMap.GetTileIndex(0,-distance);
                break;
            default:
                result = -1;
        }
        if(AIMap.DistanceManhattan(tile, result) == distance) return result;
        else return -1;
    }
    
    
    /**
    * Get the tile on the edge of the map in a certain direction from another tile.
    *
    * @param    tile        The tile to check from
    * @param    direction   The direction to check in
    *
    * @pre  Direction.IsMainDirection(direction)
    *
    * @return   The edge tile
    */
    static function GetEdgeTileInDirection(tile, direction)
    {
        switch(direction)
        {
            case Direction.NORTHEAST:
                return AIMap.GetTileIndex(ai_instance.settings.MIN_VALID_TILE, AIMap.GetTileY(tile));
            case Direction.SOUTHEAST:
                return AIMap.GetTileIndex(AIMap.GetTileX(tile), AIMap.GetMapSizeY() - 2);
            case Direction.SOUTHWEST:
                return AIMap.GetTileIndex(AIMap.GetMapSizeX() - 2, AIMap.GetTileY(tile));
            case Direction.NORTHWEST:
                return AIMap.GetTileIndex(AIMap.GetTileX(tile), ai_instance.settings.MIN_VALID_TILE);
            default:
                throw "Invalid direction in Tile.GetEdgeTileDirection: " + direction;
        }
    }
    
    
    /**
    *   Returns the TileIndex of the tile distance away in direction from center
    */
    function GetTileDirectionDistance(center, direction, distance)
    {
        switch(direction)
        {
            case Direction.NORTH:
                local result = center + AIMap.GetTileIndex(-distance, -distance);
                if(AIMap.IsValidTile(result)) return result;
            case Direction.NORTHEAST:
                local result = center + AIMap.GetTileIndex(-distance, 0);
                if(AIMap.IsValidTile(result)) return result;
            case Direction.EAST:
                local result = center + AIMap.GetTileIndex(-distance, distance);
                if(AIMap.IsValidTile(result)) return result;
            case Direction.SOUTHEAST:
                local result = center + AIMap.GetTileIndex(0, distance);
                if(AIMap.IsValidTile(result)) return result;
            case Direction.SOUTH:
                local result = center + AIMap.GetTileIndex(distance, distance);
                if(AIMap.IsValidTile(result)) return result;
            case Direction.SOUTHWEST:
                local result = center + AIMap.GetTileIndex(distance, 0);
                if(AIMap.IsValidTile(result)) return result;
            case Direction.WEST:
                local result = center + AIMap.GetTileIndex(distance, -distance);
                if(AIMap.IsValidTile(result)) return result;
            case Direction.NORTHWEST:
                local result = center + AIMap.GetTileIndex(0, -distance);
                if(AIMap.IsValidTile(result)) return result;
            default:
                throw("Tile.GetTileDirectionDistance called with direction == " + direction);
        }
    }
    
    
    /**
    *   Returns an AITileList of all valid Tiles that would be neighbours of center on a grid where only one in every distance tiles exists
    *
    *   @param  center      TileIndex   The center tile
    *   @param  distance    int         The distance around the center tile. If 1, this function is the same as GetAdjacentTiles
    *
    *   @return AITileList
    */
    function GetNeighboursDistance(center, distance)
    {
        local neighbour_list = AITileList();
        
        /* Most of the time there will be no problem, so assume that situation and optimize for it */
        local north_tile = center + AIMap.GetTileIndex(-distance, -distance);
        local south_tile = center + AIMap.GetTileIndex(distance, distance);
        if(AIMap.IsValidTile(north_tile) && AIMap.IsValidTile(south_tile))
        {
            neighbour_list.AddTile(north_tile);                                         //north
            neighbour_list.AddTile(south_tile);                                         //south
            neighbour_list.AddTile(center + AIMap.GetTileIndex(-distance, distance));   //east
            neighbour_list.AddTile(center + AIMap.GetTileIndex(distance, -distance));   //west
            neighbour_list.AddTile(center + AIMap.GetTileIndex(distance, 0));           //southwest
            neighbour_list.AddTile(center + AIMap.GetTileIndex(-distance, 0));          //northeast
            neighbour_list.AddTile(center + AIMap.GetTileIndex(0, distance));           //southeast
            neighbour_list.AddTile(center + AIMap.GetTileIndex(0, -distance));          //northwest
            return neighbour_list;
        }
        /* Alright so we're at some edge. I don't feel like doing any other optimization so just check everything */
        local neighbour = center + AIMap.GetTileIndex(-distance, -distance);            //north
        if(AIMap.IsValidTile(neighbour)) neighbour_list.AddTile(neighbour);
        local neighbour = center + AIMap.GetTileIndex(-distance, 0);                    //northeast
        if(AIMap.IsValidTile(neighbour)) neighbour_list.AddTile(neighbour);
        local neighbour = center + AIMap.GetTileIndex(-distance, distance);             //east
        if(AIMap.IsValidTile(neighbour)) neighbour_list.AddTile(neighbour);
        local neighbour = center + AIMap.GetTileIndex(0, distance);                     //southeast
        if(AIMap.IsValidTile(neighbour)) neighbour_list.AddTile(neighbour);
        local neighbour = center + AIMap.GetTileIndex(distance, distance);              //south
        if(AIMap.IsValidTile(neighbour)) neighbour_list.AddTile(neighbour);
        local neighbour = center + AIMap.GetTileIndex(distance, 0);                     //southwest
        if(AIMap.IsValidTile(neighbour)) neighbour_list.AddTile(neighbour);
        local neighbour = center + AIMap.GetTileIndex(distance, -distance);             //west
        if(AIMap.IsValidTile(neighbour)) neighbour_list.AddTile(neighbour);
        local neighbour = center + AIMap.GetTileIndex(0, -distance);                    //northwest
        if(AIMap.IsValidTile(neighbour)) neighbour_list.AddTile(neighbour);
        
        return neighbour_list;
    }
    
    
    /**
     * Checks a rectangle rooted in tile_id for flatness.
     *
     * @return  bool    True iff all tiles within the rectangle are flat
     */
    static function IsFlatRectangle(tile_id, width, height)
    {
        for(local x = 0; x < width; x++)
        {
            for(local y = 0; y < height; y++)
            {
                if(AITile.GetSlope(tile_id + AIMap.GetTileIndex(x, y)) != AITile.SLOPE_FLAT) return false;
            }
        }
        
        return true;
    }
    
    
    /**
    * Get a nice string representation of the tile coordinates.
    *
    * @param    tile    The tile to convert
    *
    * @return   A string of the form "(x, y)"
    */
    function ToString(tile)
    {
        return "(" + AIMap.GetTileX(tile) + "," + AIMap.GetTileY(tile) + ")";
    }
}