class Direction
{
    static NORTH = 0;
    static NORTHEAST = 1;
    static EAST = 2;
    static SOUTHEAST = 3;
    static SOUTH = 4;
    static SOUTHWEST = 5;   
    static WEST = 6;
    static NORTHWEST = 7;
    static INVALID_DIRECTION = 8;
    static NUM_DIRECTIONS = 8;
    
    static direction_names = ["north", "northeast", "east", "southeast", "south", "southwest", "west", "northwest", "INVALID DIRECTION"];
    
    /**
    * Returns the direction (N, NE, E, ...) from from_tile to to_tile
    *
    * @param    from_tile   The tile from which the direction to to_tile is to be determined
    * @param    to_tile     The tile to which the direction from from_tile is to be determined
    *
    * @return The correct Direction flag
    */
    static function GetDirection(from_tile, to_tile)
    {
        if(from_tile == to_tile) return Direction.INVALID_DIRECTION;
        local f_x = AIMap.GetTileX(from_tile);
        local f_y = AIMap.GetTileY(from_tile);
        local t_x = AIMap.GetTileX(to_tile);
        local t_y = AIMap.GetTileY(to_tile);
        local dist_x = abs(f_x - t_x);
        local dist_y = abs(f_y - t_y);
        
        if(dist_y <= dist_x/2)
        {
            /* The connecting line is within a cone surrounding the SW-NE line */
            if(f_x > t_x) return Direction.NORTHEAST;
            else return Direction.SOUTHWEST;
        }
        else if(dist_x <= dist_y/2)
        {
            /* The connecting line is within a cone surrounding the SE-NW line */
            if(f_y > t_y) return Direction.NORTHWEST;
            else return Direction.SOUTHEAST;
        }
        /* This should read if(dist_y > dist_x/2 && dist_y < dist_x*1.5) or something, but since those 'half quadrants' have already been covered, there's no need
            Only the four directions N, E, S, W remain. So now all that matters is the sign of the x and y differences */
        else if(f_x > t_x)
        {
            if(f_y > t_y) return Direction.NORTH;
            else return Direction.EAST;
        }
        else if(f_y > t_y) return Direction.WEST;
        else return Direction.NORTH;
    }
    
    
    /**
    * Increments or decrements the direction by ninety degrees. So if clockwise is true, north becomes east. If
    * clockwise is false, southeast becomes northeast.
    *
    * @param    direction   The direction to be turned
    * @param    clockwise   True for clockwise turning, false for counterclockwise
    *
    * @pre  Direction.IsValid(direction)
    *
    * @return   The resulting direction
    */
    static function Turn90Deg(direction, clockwise)
    {
        if(clockwise) return (direction + 2) % Direction.NUM_DIRECTIONS;
        else return (direction + Direction.NUM_DIRECTIONS - 2) % Direction.NUM_DIRECTIONS; // Negative modulo doesn't work
    }
    
    
    /**
    * Checks if direction is a valid direction.
    *
    * @param    direction   The direction to check
    *
    * @return   true if direction is valid, false otherwise
    */
    static function IsValid(direction)
    {
        return direction >= 0 && direction <= Direction.NUM_DIRECTIONS;
    }
    
    
    /**
    * Get the opposite direction.
    *
    * @param    direction   The direction to reverse
    *
    * @pre  Direction.IsValid(direction)
    *
    * @return   The opposite direction
    */
    static function Opposite(direction)
    {
        return (direction + 4) % Direction.NUM_DIRECTIONS;
    }
    
    
    /**
    * Get a nice string representation of the direction.
    *
    * @param    direction   The direction to convert
    *
    * @pre  Direction.IsValid(direction)
    *
    * @return   A lowercase string representation of the direction
    */
    function ToString(direction)
    {
        return Direction.direction_names[direction];
    }
}