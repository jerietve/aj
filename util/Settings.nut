class Settings
{
    _min_valid_tile = null;
    
    
    constructor()
    {
    }
    
    
    function Init()
    {
        /* The lowest X and Y value at which tiles are still valid is determined by the freeform_edges setting. Because
        * I'm not sure about the situation in which the setting does not exist, be on the safe side in that case and do
        * not build on the edges. */
        if(AIGameSettings.IsValid("freeform_edges") && !AIGameSettings.GetValue("freeform_edges")) _min_valid_tile = 0;
        else _min_valid_tile = 1;
    }
}