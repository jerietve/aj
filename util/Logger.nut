class Logger
{
    /**
     * Prints every item/value pair in the AIList.
     *
     * @return  nothing
     */
    static function PrintAIList(list)
    {
        AILog.Info("----AIList----");
        foreach(item, value in list)
        {
            AILog.Info(item + " / " + value);
        }
    }
    
    
    /**
     * Prints every tile/value pair in the AIList.
     *
     * @return  nothing
     */
    static function PrintAITileList(list)
    {
        AILog.Info("----Tile list----");
        foreach(tile_id, value in list)
        {
            AILog.Info(Tile.ToString(tile_id) + " / " + value);
        }
    }
}