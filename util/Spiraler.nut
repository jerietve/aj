/**
* The Spiraler takes a tile and direction to start, as well as a clockwise or counterclockwise command. It then walks
* out in a spiral from this starting tile according to the initial direction and the rotation command. The way this is
* implemented is as follows:
*
* Imagine an easier map where we just walk up, down, left and right. We start off to the right, spiraling
* counterclockwise. We take one step right. Then one step up. Then two steps left. Two steps down. Three steps right.
* Three steps up. Four steps left. See a pattern? That's how the spiraler works.
*/
class Spiraler
{
    tile = null;            // The current tile
    direction = null;       // The current direction
    clockwise = null;       // Clockwise or counterclockwise?
    series_steps = null;    // The number of steps in this series
    steps_taken = null;     // The number of steps already taken in this series
    second_series = null;   // Is this the first or the second series with this amount of steps?
    
    constructor(start_tile, direction = Direction.SOUTHEAST, clockwise = true)
    {
        this.tile = start_tile;
        this.direction = direction;
        this.clockwise = clockwise;
        this.series_steps = 1;
        this.steps_taken = 0;
        this.second_series = false;
    }
    
    
    /**
    * Move to the next tile and return it.
    *
    * @return   The tile the spiraler ended up at
    */
    function Next()
    {
        /* Check if we need to turn */
        if(steps_taken == series_steps)
        {
            direction = Direction.Turn90Deg(direction, clockwise);
            steps_taken = 0;
            /* Check if we need to increase the stretch length */
            if(second_series)
            {
                series_steps++;
            }
            second_series = !second_series;
        }
        local next_tile = Tile.GetAdjacentTile(tile, direction);
        /* Check if we would walk off the map */
        if(!AIMap.IsValidTile(next_tile))
        {
            /* We hit the edge of the map. The next series needs to be skipped, and we'll start on a later position on 
            * the series after that. Unless we're in a really messed up situation and we're exactly in a corner of the
            * map. */
            if(second_series) next_tile = Tile.GetTileInDirection(tile, Direction.Turn90Deg(direction, clockwise), series_steps + 1);
            else next_tile = Tile.GetTileInDirection(tile, Direction.Turn90Deg(direction, clockwise), series_steps);
            if(AIMap.IsValidTile(next_tile))
            {
                /* Phew, just one edge to deal with */
                tile = next_tile;
                direction = Direction.Opposite(direction);
                steps_taken = series_steps - steps_taken;
                series_steps++;
            }
            else
            {
                /* We need to skip two series */
                next_tile = Tile.GetTileInDirection(Tile.GetEdgeTileInDirection(tile, Direction.Turn90Deg(direction, clockwise)),
                        Direction.Opposite(direction), steps_taken + 1);
                if(second_series)
                {
                    steps_taken = series_steps - Distance.BetweenTilesDirection(tile, next_tile, Direction.Turn90Deg(direction, clockwise)) + 1;
                    series_steps += 2;
                }
                else
                {
                    steps_taken = series_steps - Distance.BetweenTilesDirection(tile, next_tile, Direction.Turn90Deg(direction, clockwise));
                    series_steps++;
                }
                second_series = !second_series;
                direction = Direction.Turn90Deg(direction, !clockwise);         
                tile = next_tile;
            }
        }
        else
        {
            tile = next_tile;
            steps_taken++;
        }
        
        return tile;
    }
}