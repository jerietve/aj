class Heuristics
{
    /**
    * Calculates the flatness heuristic for an area surrounding a tile. It is the sum of the difference with the average
    * height at every point in the rectangle.
    *
    * @param    center      The tile arround which to check
    * @param    radius_x    The amount of tiles away from the center to include in positive and negative x-direction
    * @param    radius_y    The amount of tiles away from the center to include in positive and negative y-direction
    *
    * @return   The flatness value
    */
    static function Flatness(center, radius_x, radius_y = null)
    {
        if(radius_y == null) radius_y = radius_x;
        
        /* Get the corner tiles of the rectangle */
        local extremes = Util.GetExtremesRectAroundTile(center, radius_x, radius_y);
        local x_min = extremes[0];
        local x_max = extremes[1];
        local y_min = extremes[2];
        local y_max = extremes[3];
        
        /* Get all corner heights and save them in an array */
        local heights = [];
        for(local x = x_min; x <= x_max; x++)
        {
            for(local y = y_min; y <= y_max; y++)
            {
                heights.push(AITile.GetCornerHeight(AIMap.GetTileIndex(x, y), AITile.CORNER_N));
                if(x == x_max) heights.push(AITile.GetCornerHeight(AIMap.GetTileIndex(x, y), AITile.CORNER_W));
            }
            heights.push(AITile.GetCornerHeight(AIMap.GetTileIndex(x, y_max), AITile.CORNER_E));
        }
        heights.push(AITile.GetCornerHeight(AIMap.GetTileIndex(x_max, y_max), AITile.CORNER_S));
        
        /* Get the average, then the sum of all differences */
        local average = Math.AverageArray(heights);
        local diffSum = 0.0;
        for(local i = 0; i < heights.len(); i++) diffSum += fabs(heights[i] - average);
        return diffSum;
    }
}