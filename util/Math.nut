class Math
{
    /**
    * Get the average of the values in an array.
    *
    * @param    in_array    An array of numbers
    *
    * @return   The average
    */
    static function AverageArray(in_array)
    {
        local sum = 0;
        for(local i = 0; i < in_array.len(); i++) sum += in_array[i];
        return sum * 1.0 / in_array.len();
    }


    static function Maximum(a, b)
    {
        if(a > b) return a;
        else return b;
    }


    static function Minimum(a, b)
    {
        if(a < b) return a;
        else return b;
    }
}